from google.appengine.ext.appstats import recording

import web

from palimpsest.pages import \
Index, About, LeaveFeedback, \
Account, AccountDelete, AccountWorlds, AccountSaves, AccountSave, ModeratorAuth, \
WorldCover, WorldGo, WorldSave, WorldEdit, \
ReloadMenu, WorldCreate, PropertyCreate, TextCreate, ModeratorCreate, \
PropertyEdit, TextEdit
from palimpsest.utils.handle_errors import notfound, internalerror, forbidden

urls = (
     '/', 'Index'
    ,'/about', 'About'
    ,'/feedback', 'LeaveFeedback'
    ,'/you', 'Account'
    ,'/you/delete', 'AccountDelete'
    ,'/yours', 'AccountWorlds'
    ,'/yours/saves', 'AccountSaves'
    ,'/yours/load', 'ReloadMenu'
    ,'/yours/new', 'WorldCreate'
    ,'/journey/(\d+)', 'AccountSave'
    ,'/(.*)/auth/(.*)', 'ModeratorAuth'
    ,'/(.*)/go', 'WorldGo'
    ,'/(.*)/save/(\d+)', 'WorldSave'
    ,'/(.*)/edit', 'WorldEdit'
    ,'/(.*)/property', 'PropertyCreate'
    ,'/(.*)/property/(\d+)', 'PropertyEdit'
    ,'/(.*)/properties', 'PropertyCreate'
    ,'/(.*)/text', 'TextCreate'
    ,'/(.*)/text/(\d+)', 'TextEdit'
    ,'/(.*)/texts', 'TextCreate'
    ,'/(.*)/mods', 'ModeratorCreate'
    ,'/(.*)', 'WorldCover'
)

app = web.application(urls, globals())

app.notfound = notfound
app.internalerror = internalerror
app.forbidden = forbidden

application = recording.appstats_wsgi_middleware(app.wsgifunc())