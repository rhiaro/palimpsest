if (!google.maps.Polygon.prototype.getBounds) {
    google.maps.Polygon.prototype.getBounds = function(latLng) {
        var bounds = new google.maps.LatLngBounds();
        var paths = this.getPaths();
        var path;
        for (var p = 0; p < paths.getLength(); p++) {
            path = paths.getAt(p);
            for (var i = 0; i < path.getLength(); i++) {
                bounds.extend(path.getAt(i));
            }
        }
        return bounds;
    }
}

function isInPoly(poly, latLng) {
	// Exclude points outside of bounds as there is no way they are in the poly
	var bounds = poly.getBounds();

	if(bounds != null){// && !isInPoly(bounds, latLng)) {
		return false;
	}

	// Raycast point in polygon method
	var numPoints = poly.getVertexCount();
	var inPoly = false;
	var i;
	var j = numPoints-1;

	for(var i=0; i < numPoints; i++) {
		var vertex1 = poly.getVertex(i);
		var vertex2 = poly.getVertex(j);

		if (vertex1.lng() < latLng.lng() && vertex2.lng() >= latLng.lng() || vertex2.lng() < latLng.lng() && vertex1.lng() >= latLng.lng()) {
			if (vertex1.lat() + (latLng.lng() - vertex1.lng()) / (vertex2.lng() - vertex1.lng()) * (vertex2.lat() - vertex1.lat()) < latLng.lat()) {
				inPoly = !inPoly;
			}
		}

		j = i;
	}

	return inPoly;
};