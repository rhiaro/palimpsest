function validateEmail(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

$("#save_contrib").click(function(){
	if(!validateEmail($("#add_contrib").val())){
		event.preventDefault();
		$("<span class='fail'>Must be a valid email</span>").insertBefore(this);
	}	
});