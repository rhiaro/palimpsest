if (!google.maps.Polygon.prototype.getBounds) {
    google.maps.Polygon.prototype.getBounds = function(latLng) {
        var bounds = new google.maps.LatLngBounds();
        var paths = this.getPaths();
        var path;
        for (var p = 0; p < paths.getLength(); p++) {
            path = paths.getAt(p);
            for (var i = 0; i < path.getLength(); i++) {
                bounds.extend(path.getAt(i));
            }
        }
        return bounds;
    }
}

google.maps.Polygon.prototype.containsLatLng = function(latLng) {

    // Raycast point in polygon method

    var numPoints = this.getPath().getLength();
    var inPoly = false;
    var i;
    var j = numPoints-1;

    for(var i=0; i < numPoints; i++) {
    	var vertex1 = this.getPath().getAt(i);
    	var vertex2 = this.getPath().getAt(j);

    	if (vertex1.lng() < latLng.lng() && vertex2.lng() >= latLng.lng() || vertex2.lng() < latLng.lng() && vertex1.lng() >= latLng.lng()) {
    		if (vertex1.lat() + (latLng.lng() - vertex1.lng()) / (vertex2.lng() - vertex1.lng()) * (vertex2.lat() - vertex1.lat()) < latLng.lat()) {
    			inPoly = !inPoly;
    		}
    	}

    	j = i;
    }

    return inPoly;
};
