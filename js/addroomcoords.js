  var poly, map;
  var markers = [];
  var path = new google.maps.MVCArray;
  var seq = 0;

  function initialize() {
    var ed = new google.maps.LatLng(55.94580550893789, -3.17979097366333);

    map = new google.maps.Map(document.getElementById("map-canvas"), {
      zoom: 14,
      center: ed,
      mapTypeId: google.maps.MapTypeId.SATELLITE
    });

    poly = new google.maps.Polygon({
      strokeColor: '#66d9ef',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#66d9ef',
      fillOpacity: 0.35
    });
    poly.setMap(map);
    poly.setPaths(new google.maps.MVCArray([path]));

    google.maps.event.addListener(map, 'click', addPoint);

    currentRooms(rooms, map);
  }

  function addPoint(e) {
    path.insertAt(path.length, e.latLng);

    var marker = new google.maps.Marker({
      position: e.latLng,
      map: map,
      draggable: true
    });
    seq++;
    markers.push(marker);
    marker.setTitle("#" + seq);
    
    var latLngStr = String(e.latLng);
    var latLng = latLngStr.split(", ");
    var lat = latLng[0].substring(1);
    var lng = latLng[1].substring(0, latLng[1].length-1)
    var latInput = '<input type="hidden" value="'+lat+'" name="lat" id="lat'+seq+'" />';
    var lngInput = '<input type="hidden" value="'+lng+'" name="long" id="long'+seq+'" />';
    document.getElementById('coordInputs').innerHTML += latInput + lngInput;

    google.maps.event.addListener(marker, 'dragend', function() {
      for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
      path.setAt(i, marker.getPosition());
      var co = i+1;
      //document.getElementById('co'+co).innerHTML = co + ": " + marker.getPosition();
      var latLngStr = String(marker.getPosition());
      var latLng = latLngStr.split(", ");
      var lat = latLng[0].substring(1);
      var lng = latLng[1].substring(0, latLng[1].length-1)
      document.getElementById('lat'+co).value = lat;
      document.getElementById('long'+co).value = lng;
      }
    );
  }
  
  function currentRooms(rooms, map){
    for(var i = 0; i < rooms.length; i++) {
      var shape = new google.maps.Polygon({
        paths: rooms[i],
        strokeColor: '#f92672',
        strokeOpacity: 0.4,
        strokeWeight: 1,
        fillColor: '#f92672',
        fillOpacity: 0.15
      });

      shape.setMap(map);

      google.maps.event.addListener(shape, 'click', addPoint);
    }
  }

  jQuery('#roomcreate').submit(function() {
    if(!jQuery('#lat1').val() || !jQuery('#long1').val()){
      alert('You forgot to use the map to add a location!');
      return false;
    }
  });

  google.maps.event.addDomListener(window, 'load', initialize);