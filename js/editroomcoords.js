var markers = [];
  var poly, map;
  var path = new google.maps.MVCArray;
  var seq = 0;

  //var testedit = [new google.maps.LatLng(55.94060231848573, -3.179173850803636),new google.maps.LatLng(55.94101090838416, -3.177628898411058),new google.maps.LatLng(55.94048214416592, -3.1771353719523177),new google.maps.LatLng(55.9400975838369, -3.178594493656419)];

  function initialize() {
    var edin = new google.maps.LatLng(55.94580550893789, -3.17979097366333);

    map = new google.maps.Map(document.getElementById("map-canvas"), {
      zoom: 14,
      center: edin,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
    });

    poly = new google.maps.Polygon({
      strokeColor: '#66d9ef',
      strokeOpacity: 0.8,
      strokeWeight: 1,
      fillColor: '#66d9ef',
      fillOpacity: 0.35
    });
    poly.setMap(map);
    poly.setPaths(new google.maps.MVCArray([path]));

    currentRooms(rooms, map);

    if(toEdit.length > 0){
        editingRoom = editRoom(toEdit);
        editingRoom.setMap(map);

        ed = editingRoom.getPath();

        var deleteNode = function(mev) {
          if (mev.vertex != null) {
            ed.removeAt(mev.vertex);
          }
        }
        google.maps.event.addListener(editingRoom, 'rightclick', deleteNode);
    }else{
      google.maps.event.addListener(map, 'click', addPoint);
    }

  }
  
  function currentRooms(rooms, map){
    for(var i = 0; i < rooms.length; i++) {
      var shape = new google.maps.Polygon({
        paths: rooms[i],
        strokeColor: '#f92672',
        strokeOpacity: 0.4,
        strokeWeight: 1,
        fillColor: '#f92672',
        fillOpacity: 0.15
      });

      shape.setMap(map);
    }
  }

  function editRoom(room){
      var shape = new google.maps.Polygon({
        paths: room,
        strokeColor: '#66d9ef',
        strokeOpacity: 0.8,
        strokeWeight: 1,
        fillColor: '#66d9ef',
        fillOpacity: 0.35,
        editable: true
      });

      return shape;
  }

  function addPoint(e) {
    path.insertAt(path.length, e.latLng);

    var marker = new google.maps.Marker({
      position: e.latLng,
      map: map,
      draggable: true
    });
    markers.push(marker);

    google.maps.event.addListener(marker, 'dragend', function() {
      for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
      path.setAt(i, marker.getPosition());
      }
    );
  }

  jQuery('#roomcreate').submit(function() {
    inputs = "";
    if(toEdit.length == 0){
      for(var i = 0; i < markers.length; i++){
        var latInput = '<input type="hidden" value="'+markers[i].getPosition().lat()+'" name="lat" id="lat'+i+'" />';
        var lngInput = '<input type="hidden" value="'+markers[i].getPosition().lng()+'" name="long" id="long'+i+'" />';
        var seqInput = '<input type="hidden" value="' + i + '" name="seq" id="seq'+i+'" />';
        inputs += latInput + lngInput + seqInput;
      }

    }else{
      for(var i = 0; i < ed.getArray().length; i++){
        var latInput = '<input type="hidden" value="'+ed.getAt(i).lat()+'" name="lat" id="lat'+i+'" />';
        var lngInput = '<input type="hidden" value="'+ed.getAt(i).lng()+'" name="long" id="long'+i+'" />';
        var seqInput = '<input type="hidden" value="' + i + '" name="seq" id="seq'+i+'" />';
        inputs += latInput + lngInput + seqInput;
      }
    }
    
    jQuery('#coordInputs').html(inputs);
    //alert(inputs);
    //return false;
    /*if(!jQuery('#lat1').val() || !jQuery('#long1').val()){
      alert('You forgot to use the map to add a location!');
      return false;
    }*/
  });

  google.maps.event.addDomListener(window, 'load', initialize);