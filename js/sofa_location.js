var map;
function initialize() {
  var ed = new google.maps.LatLng(55.95017300554032, -3.187655210494995);
  //high st - 55.95017300554032, -3.187655210494995
  //brown st - 55.94580550893789, -3.17979097366333

  jQuery("#sofa-map").height("300px");
  jQuery("#sofa-map").width("100%");
  jQuery("#loadnext").hide();
  jQuery("#playpause").hide();
  jQuery("#loading").html("Move the map to search for texts, just as if you were walking down the street. (Except probably a bit faster, and with less chance of rain.");

  map = new google.maps.Map(document.getElementById("sofa-map"), {
    zoom: 18,
    disableDoubleClickZoom: true,
    scrollwheel: false,
    panControl: false,
    zoomControl: false,
    center: ed,
    mapTypeId: google.maps.MapTypeId.HYBRID
  });

  var marker = new google.maps.Marker({
    position: map.getCenter(),
    icon: "/img/footprintmarker.png",
    map: map
  });

  //google.maps.event.addListener(map, 'click', dostuff);
  google.maps.event.addListener(map, 'bounds_changed', function(){
    marker.setPosition(map.getCenter());
    dostuff();
  });

}

function getRoom(lat, lng) {
  var coord = new google.maps.LatLng(lat,lng);

  for(var i=0; i < rooms.length; i++){
    var polygon = new google.maps.Polygon({
        paths: rooms[i]
    });

    var inside = polygon.containsLatLng(coord);
    if(inside){
      return i;
    }
  }
  return false;
}

function appendToPage(data){
  jQuery("#loading").hide();
  jQuery('#page>.inner').append(data).fadeIn('slow');
  jQuery('#page>.inner').scrollTo(jQuery('#page .inner h3').filter(':last'), {duration: 2000});
}

function prependToPage(data){
  jQuery("#loading").hide();
  jQuery('#page>.inner').prepend(data).fadeIn('slow');
  jQuery('#page>.inner').scrollTo(jQuery('#page .inner h3').filter(':first'), {duration: 2000});
}

function setInfo(data){
  jQuery('#info .inner').html(data);
}

var lastid = "-";

function dostuff(event) {
  //var latitude = event.latLng.lat();
  //var longitude = event.latLng.lng();
  var center = map.getCenter();
  var latitude = center.lat();
  var longitude = center.lng();

  //alert("( " + latitude + ", " + longitude + ")");

  var in_room = getRoom(latitude,longitude);
  //jQuery('#page .inner').prepend("<p>" + roomIds[in_room] + " " + dump + "</p>");
  if(in_room !== false){

    if(roomIds[in_room] != lastid){
      $.ajax({
        type: "POST",
        url: postUrl,
        data: { key: roomIds[in_room] }
      }).done(function(data) {
        
        data = jQuery.parseJSON(data);
        prependToPage(data.room);
        setInfo(data.roominfo);

      });
    }
    lastid = roomIds[in_room];

  /*}else{
    if(lastid != null){
      appendToPage("<p class='color3' style='font-size:0.8em'>" + voidText + "</p>");
    }
    lastid = null;*/
  }

  /*var params = getParameters();
  var world = params['w'];*/
}

google.maps.event.addDomListener(window, 'load', initialize);