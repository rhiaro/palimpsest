var play = false;
var queue = Array();
var first = true;

jQuery('#playpause').click(function(){
	if(play){
		jQuery(this).removeClass("lighter").addClass("color3");
		jQuery(this).find("i").removeClass("icon-play").addClass("icon-pause");
		play = false;
	}else{
		jQuery(this).addClass("lighter").removeClass("color3");
		jQuery(this).find("i").addClass("icon-play").removeClass("icon-pause");
		addAll()
		play = true;
	}
});

function addAll(){
	setInterval(function() { if (queue.length > 0) { addNext(); } }, 500);
}

function addNext(){
	if(queue.length > 0){
		next = queue.pop();
		appendToPage(next.room);
	    setInfo(next.roominfo);
	}
	jQuery('#no').text(queue.length);
}

jQuery('#loadnext').click(function(){addNext()});