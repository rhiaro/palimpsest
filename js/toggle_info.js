var hidden = true;
$('#getinfo').click(function() {
	if(hidden){
		$("#info").show();
		hidden = false;
        $(this).find("i").attr("class", "icon-remove-circle");
	}else{
		$("#info").hide();
		hidden = true;
        $(this).find("i").attr("class", "icon-info-sign");
	}
	
  return false;
});