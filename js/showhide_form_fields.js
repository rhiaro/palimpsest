if(jQuery('#p_opts').find('option:selected').val() != 0){
	jQuery('#p_vals').hide();
	jQuery('#p_vals').prev('label').hide();
}
jQuery('#p_opts').change(function() {
	var optval = jQuery(this).find('option:selected').val();
	if(optval == 0){
		jQuery('#p_vals').show();
		jQuery('#p_vals').prev('label').show();
	}else{
		jQuery('#p_vals').hide();
		jQuery('#p_vals').prev('label').hide();
	}
});

jQuery(function(){
	jQuery('#sorter_True').change(function(){
		jQuery('#discrete').parent('p').toggle(this.checked);
	}).change();
});