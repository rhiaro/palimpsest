$('.del_property').change(function() {
 $(this).next('span').toggleClass('strike');
})

function get_input_type(valid_val){
    if(valid_val == "|text|" || valid_val == "|num|"){
        var property_input = "<input type=\"text\" name=\"prop_val\" id=\"prop_val\" style=\"display:inline; max-width:16em\"/>";
    }else{
        var vals = valid_val.split('|');
        var property_input = "<select id=\"prop_val\" name=\"prop_val\">";
        for(value in vals){
            property_input += "<option value=\"" + vals[value] +"\">" + vals[value] + "</option>";
        }
        property_input += "</select>";
    }
    return property_input;
}