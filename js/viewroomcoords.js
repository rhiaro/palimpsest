var map;

  function initialize() {

    map = new google.maps.Map(document.getElementById("map-canvas"), {
      zoom: 14,
      center: aRoom[0],
      mapTypeId: google.maps.MapTypeId.SATELLITE
    });

    setRoom(aRoom, map);

  }
  
  function setRoom(room, map){
      var shape = new google.maps.Polygon({
        paths: aRoom,
        strokeColor: '#f92672',
        strokeOpacity: 0.4,
        strokeWeight: 1,
        fillColor: '#f92672',
        fillOpacity: 0.15
      });

      shape.setMap(map);
  }

  google.maps.event.addDomListener(window, 'load', initialize);