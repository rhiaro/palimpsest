var map;
function initialize() {
  var ed = new google.maps.LatLng(55.95017300554032, -3.187655210494995);

  map = new google.maps.Map(document.getElementById("map-canvas"), {
    zoom: 14,
    center: ed,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  });

  currentRooms(rooms, map);
}

function currentRooms(rooms, map){
  for(var i = 0; i < rooms.length; i++) {
    var shape = new google.maps.Polygon({
      paths: rooms[i],
      strokeColor: '#f92672',
      strokeOpacity: 0.4,
      strokeWeight: 1,
      fillColor: '#f92672',
      fillOpacity: 0.15
    });

    shape.setMap(map);
  }
}

google.maps.event.addDomListener(window, 'load', initialize);