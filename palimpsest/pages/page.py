from google.appengine.api import users
from datetime import date

import web
import re

from palimpsest.models import world_exists
from palimpsest.logins import ensure_logged_in, ensure_user_exists, get_login_url, get_logout_url, get_current_user

class Page(object):

    page_user = None
    
    def __init__(self):

        """ Get our user if a Google user is logged in. """
        user = users.get_current_user()
        if user:
            self.page_user = ensure_user_exists(user)

        """ Give a Page instance require_login=True, non-logged-in users will be redirected to login page."""
        try:
            if self.require_login:
                ensure_logged_in()
            else:
                # Do something here for pages that require you to be logged out
                pass
        except AttributeError: # The page does not require anything
            pass

    def redirect_if_not_exists(self, thing, url="/yours"):
        if thing is None:
            raise web.seeother(url)


""" Templates stuff """

def render():
    return web.template.render('palimpsest/pages/templates/', globals=glob())

def glob():
    """ Get globally available global variables dict. """
    glob = {
         'encode':encode
        ,'top_big':render_top_big
        ,'top_small':render_top_small
        ,'end':render_end
        ,'cur_user':get_current_user
        ,'print_cur_user':print_current_user
        ,'login_url':get_login_url
        ,'logout_url':get_logout_url
        ,'date':make_nice_date
        ,'page':web.ctx.path
    }
    return glob

def get_login_or_out_url():
    if(users.get_current_user()):
        link = users.create_logout_url(web.ctx.path)
        linktxt = 'Logout ('+user.nickname()+')'
    else:
        link = users.create_login_url(web.ctx.path)
        linktxt = 'Login'

    loglink = {loglink:(linktxt, link)}
    return loglink

def print_current_user():
    if get_current_user():
        return get_current_user().printable()
    return " there"

def render_nav(**kwargs):
    """ Build the navigation. """

    keys = sorted(kwargs.keys())

    ul = []
    for kw in keys:
        ul.append('<a href="%s">%s</a>' % (kwargs[kw][1], kwargs[kw][0]))

    return render().nav(ul)

def render_twopart_nav(l, r):
    left = []
    right = []
    left.append('<a href="%s"><i class="icon-circle-arrow-left" title="%s"></i></a>' % (l[0][1], l[0][0]))
    for x in range(1,len(l)):
        left.append('<a href="%s">%s</a>' % (l[x][1], l[x][0]))
    for each in r:
        right.append('<a href="%s">%s</a>' % (each[1], each[0]))

    return render().base_nav_narrow(left, right)

def render_top_big(main_nav, title="Palimpsest", user_nav=[]):
    main_nav = render_nav(**main_nav)
    user_nav = render().base_nav_user(user_nav)
    return render().base_top_big(title, main_nav, user_nav)

def render_top_small(navleft, navright, title="Palimpsest"): #navleft and navright are lists of tuples as (text, url)
    nav = render_twopart_nav(navleft, navright)
    return render().base_top_small(title, nav)

def render_end(pre=0,js="",post=0):
    return render().base_end(pre,js,post)


""" String maniuplation stuff """

def encode(string):
    return string.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;').replace("'", '&#39;')

def make_slug(from_string):
    r = re.compile(r"[^a-zA-Z0-9-]+")
    return r.sub("",from_string.lower().replace(' ','-'))

def make_world_slug(from_string, i=1):
    try_slug = make_slug(from_string)
    if world_exists(try_slug) is None and (try_slug != "literary" and try_slug != "you" and try_slug != "yours" and try_slug != "about" and try_slug != "feedback"):
        return try_slug
    else:
        if i > 1:
            reset_slug = try_slug.rpartition('-')
            try_slug = reset_slug[0]
        try_slug = "%s-%d" % (try_slug, i)
        i += 1
        return make_world_slug(try_slug, i)

def make_nice_date(date):
    return date.strftime("%A %d %B %Y")
