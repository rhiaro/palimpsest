from web.template import CompiledTemplate, ForLoop, TemplateResult

import old
# coding: utf-8
def about (title="About Palimpsest", main_nav={"main":("Home", "/")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title=title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'                <div class="wrapper">\n'])
    extend_([u'                        <div class="w1of2">\n'])
    extend_([u'                                <div class="inner align-left clearfix">\n'])
    extend_([u'                                        <h2>About Palimpsest: Literary Edinburgh</h2>\n'])
    extend_([u"                                        <p>'Palimpsest: Literary Edinburgh' originally arose out of the idea of creating an innovative new way of engaging people with literature to celebrate the 250th anniversary of the University of Edinburgh's English Literature department, the oldest in the world. For while the study of English Literature is now well established worldwide, the appointment of the Reverend Hugh Blair as the first Regius Professor of Rhetoric and Belles Lettres at the University of Edinburgh in 1762 was then an innovation. A little over a decade later, Edward Topham in his book <em>Letters from Edinburgh</em> in describing Blair tells of '[t]he harmony of his diction, the elegance and sagacity of his criticisms [and] the spirit and fire of his imagination'.\n"])
    extend_([u"                                        <p>Topham then goes on to commend the uniqueness of the University, remarking that if the general nature of its education were followed by more institutions it would 'be a considerable step towards the advancement of learning and literature' (215-16). One of the ways the department aims to perpetuate that educational and imaginative spirit is through Palimpsest a mobile web application that will weave together dramatic, vivid and evocative extracts of fictional and historical literary texts set in Edinburgh, thereby making these texts newly accessible and responsive to the scholarly and creative impulses of scholars, writers and the general public.</p>\n"])
    extend_([u"                                        <p>The particular capacity of Edinburgh and its literature when combined to fire one's imagination, is described by Robert Louis Stevenson in his <em>Picturesque Notes</em>:</p>\n"])
    extend_([u"                                        <blockquote><p>So, in the low dens and high-flying garrets of Edinburgh, people may go back upon dark passages in the town's adventures, and chill their marrow with winter's tales about the fire: tales that are singularly apposite and characteristic, not only of the old life, but of the very constitution of built nature in that part, and singularly well qualified to add horror to horror, when the wind pipes around the tall LANDS, and hoots adown arched passages, and the far-spread wilderness of city lamps keeps quavering and flaring in the gusts. (15)</p></blockquote>\n"])
    extend_([u'                                        <p>Stevenson then goes on to conjure chilling tales, some now more familiar than others, of Deacon Brodie, Begbie the Porter, and Burke and Hare. More recently, Ian Rankin has in turn jointly credited Stevenson and Edinburgh with the inspiring of his own fiction:</p>\n'])
    extend_([u"                                        <blockquote><p>I owe a great debt to Robert Louis Stevenson and to the city of his birth. In a way they both changed my life. Without Edinburgh's split nature Stevenson might never have dreamed up <em>Dr Jekyll and Mr Hyde</em> and without <em>Dr Jekyll and Mr Hyde</em> I might never have come up with my own alter ego Detective Inspector John Rebus (qtd. in <em>The Evening News</em>)</p></blockquote>\n"])
    extend_([u'                                </div>\n'])
    extend_([u'                        </div>\n'])
    extend_([u'\n'])
    extend_([u'                        <div class="w1of2"><div class="inner align-left clearfix">\n'])
    extend_([u"                                <p>So, as with Stevenson, in Rankin's debt of gratitude are intertwined the nature of the city itself and the fiction it has inspired. The literature is shot through with the city, and the city with the literature, as they reciprocally shape each other's forms and histories.</p>\n"])
    extend_([u"                                <p>For the prototype database, staff across the English Literature department contributed excerpts of texts set in the High Street that range from the medieval period to the current day. Next the application has been made openly accessible, with the intention that much of its growth will now come from users uploading further texts to the database themselves. Texts can be organised according to a range of properties, although these will vary according to what is known, they can include: setting, author, title of work, date of setting or publication, genre, topic, mood, weather, season, day and time of day. The aim is that through the geolocation of the texts to their settings the app will create imaginary and historical cityscapes, which will enable users to immerse themselves in a random psychogeography composed of non-linear fragments, or to choose their journey according to the various properties, such as a specific author, genre, period, or mood. Users can also save the specific literary journeys they have undertaken and add to the listed properties for an existing text. While the idea was borne out of the department's anniversary and the richness of Edinburgh's literary history, a future possibility might be for the Palimpsest application to be recreated in different cities or places and so offer 'travel through texts' all around the world. Another future form of development will come through involving creative writers and the users of the application in exploring tropes of interactive fiction or storytelling, with both the University and the city of today providing a wealth of experience in the literary arts. Thus in these diverse ways Palimpsest seeks to bring texts to life and evoke the multi-layered imaginative, conceptual and historical cityscape of our everyday settings.</p>\n"])
    extend_([u'                                <p class="align-right"><strong>Words by Miranda Anderson</strong></p>\n'])
    extend_([u'                                <h3>The Team</h3>\n'])
    extend_([u'                                <p>Initiator &amp; Conceptual director: Miranda Anderson</p>\n'])
    extend_([u'                                <p>Technical director, front- &amp; back-end developer: <a href="http://rhiaro.co.uk" target="_blank">Amy Guy</a></p>\n'])
    extend_([u'                                <p>Main academic team: Simon Biggs, John Lee, James Loxley, Mark Wright, <a href="http://www.eca.ac.uk/circle/" target="_blank">CIRCLE</a> &amp; UoE English Literature Department</p>\n'])
    extend_([u'                                <p>Design &amp; Digital Media project students: Bing Liu, Haiyan Pan, Jessica Ruiz, Bethany Wolfe, Qingwen Xie, Yi Yang, Guirong Zhang</p>\n'])
    extend_([u'                                <p class="color1"><a href="/feedback">Have questions, comments, suggestions, or any other feedback for us? Click here &#187;</a></p>\n'])
    extend_([u'                        </div></div>\n'])
    extend_([u'\n'])
    extend_([u'                </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

about = CompiledTemplate(about, 'palimpsest/pages/templates/about.html')
join_ = about._join; escape_ = about._escape

# coding: utf-8
def admin (world, section, menu={'properties':[],'texts':[],'contributors':[],'moderation':[]}, title="Palimpsest", navleft=[('Back','/yours')], navright=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_small(navleft=navleft, navright=navright, title=title), False), u'\n'])
    extend_([u'\n'])
    extend_([u'        <div class="admin-head clearfix">\n'])
    extend_([u'            <h2>', escape_(world.title, True), u'</h2>\n'])
    extend_([u'            <p>\n'])
    if world.key.id() != "default-world" and len(menu['contributors']) > 0:
        extend_(['                ', u'<a href="/', escape_(str(world.key.id()), True), u'/edit" class="btn light-hov light-border">Edit</a> \n'])
    extend_([u'                <a href="#menu" class="btn light-hov light-border mob">Menu</a>\n'])
    extend_([u'            </p>\n'])
    extend_([u'        </div>\n'])
    extend_([u'\n'])
    extend_([u'        <div class="has-side"><div class="inner align-left">\n'])
    extend_([u'            ', escape_(section, False), u'\n'])
    extend_([u'        </div></div>\n'])
    extend_([u'\n'])
    extend_([u'        <div class="side" id="menu">\n'])
    extend_([u'            <nav>\n'])
    extend_([u'                <ul class="li1">\n'])
    extend_([u'                    <!-- Properties -->\n'])
    if len(menu['properties']) > 0:
        extend_(['                    ', u'<li class="', escape_(menu['properties'][0][2], True), u'">\n'])
        extend_(['                    ', u'    <h3>\n'])
        extend_(['                    ', u'        <a class="clearfix" href="', escape_(menu['properties'][0][1], True), u'">\n'])
        extend_(['                    ', u'            <span class="left">', escape_(menu['properties'][0][0], True), u'</span><i class="right ', escape_(menu['properties'][0][3], True), u'"></i>\n'])
        extend_(['                    ', u'        </a>\n'])
        extend_(['                    ', u'    </h3>\n'])
        if len(menu['properties']) > 1:
            extend_(['                        ', u'<ul class="li2">\n'])
            for x in loop.setup(range(1,len(menu['properties']))):
                extend_(['                            ', u'<li class="', escape_(menu['properties'][x][2], True), u'">\n'])
                extend_(['                            ', u'    <a href="', escape_(menu['properties'][x][1], True), u'">', escape_(menu['properties'][x][0], True), u'\n'])
                extend_(['                            ', u'    <i class="icon-edit extra right"></i></a>\n'])
                extend_(['                            ', u'</li>\n'])
            extend_(['                        ', u'</ul>\n'])
        extend_(['                    ', u'</li>\n'])
    extend_([u'                    <!-- Texts -->\n'])
    if len(menu['texts']) > 0:
        extend_(['                    ', u'<li class="', escape_(menu['texts'][0][2], True), u'">\n'])
        extend_(['                    ', u'    <h3>\n'])
        extend_(['                    ', u'        <a class="clearfix" href="', escape_(menu['texts'][0][1], True), u'">\n'])
        extend_(['                    ', u'            <span class="left">', escape_(menu['texts'][0][0], True), u'</span><i class="right ', escape_(menu['texts'][0][3], True), u'"></i>\n'])
        extend_(['                    ', u'        </a>\n'])
        extend_(['                    ', u'    </h3>\n'])
        if len(menu['texts']) > 1:
            extend_(['                        ', u'<ul class="li2">\n'])
            for x in loop.setup(range(1,len(menu['texts']))):
                extend_(['                            ', u'<li class="', escape_(menu['texts'][x][2], True), u'">\n'])
                extend_(['                            ', u'    <a href="', escape_(menu['texts'][x][1], True), u'">', escape_(menu['texts'][x][0], True), u'\n'])
                extend_(['                            ', u'    <i class="icon-edit extra right"></i></a>\n'])
                extend_(['                            ', u'</li>\n'])
            extend_(['                        ', u'</ul>\n'])
        extend_(['                    ', u'</li>\n'])
    extend_([u'                    <!-- Contributors -->\n'])
    if len(menu['contributors']) > 0:
        extend_(['                    ', u'<li class="', escape_(menu['contributors'][0][2], True), u'">\n'])
        extend_(['                    ', u'    <h3>\n'])
        extend_(['                    ', u'        <a class="clearfix" href="', escape_(menu['contributors'][0][1], True), u'">\n'])
        extend_(['                    ', u'            <span class="left">', escape_(menu['contributors'][0][0], True), u'</span><i class="right ', escape_(menu['contributors'][0][3], True), u'"></i>\n'])
        extend_(['                    ', u'        </a>\n'])
        extend_(['                    ', u'    </h3>\n'])
        extend_(['                    ', u'</li>\n'])
    extend_([u'                    <!-- Moderation -->\n'])
    if len(menu['moderation']) > 0:
        extend_(['                    ', u'<li class="', escape_(menu['moderation'][0][2], True), u'">\n'])
        extend_(['                    ', u'    <h3>\n'])
        extend_(['                    ', u'        <a class="clearfix" href="', escape_(menu['moderation'][0][1], True), u'">\n'])
        extend_(['                    ', u'            <span class="left">', escape_(menu['moderation'][0][0], True), u'</span><i class="right ', escape_(menu['moderation'][0][3], True), u'"></i>\n'])
        extend_(['                    ', u'        </a>\n'])
        extend_(['                    ', u'    </h3>\n'])
        if len(menu['moderation']) > 1:
            extend_(['                        ', u'<ul class="li2">\n'])
            for x in loop.setup(range(1,len(menu['moderation']))):
                extend_(['                            ', u'<li class="', escape_(menu['moderation'][x][2], True), u'">\n'])
                extend_(['                            ', u'    <a href="', escape_(menu['moderation'][x][1], True), u'">', escape_(menu['moderation'][x][0], True), u'\n'])
                extend_(['                            ', u'    <i class="icon-edit extra right"></i></a>\n'])
                extend_(['                            ', u'</li>\n'])
            extend_(['                        ', u'</ul>\n'])
        extend_(['                    ', u'</li>\n'])
    extend_([u'                    <!-- Reload menu button -->\n'])
    extend_([u'                    <form method="post" action="/yours/load">\n'])
    extend_([u'                        <input type="hidden" name="refer" value="', escape_(page, True), u'" />\n'])
    extend_([u'                        <input type="hidden" name="world" value="', escape_(world.key.id(), True), u'" />\n'])
    extend_([u'                        <input type="hidden" name="reload" value="true" />\n'])
    extend_([u'                        <p><button type="submit" class="lighter-bg light-border" title="Reload the list to see if there\'s anything new"><i class="icon-refresh"></i></button></p>\n'])
    extend_([u'                    </form>\n'])
    extend_([u'                </ul>\n'])
    extend_([u'            </nav>\n'])
    extend_([u'        </div>\n'])
    extend_([u'\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

admin = CompiledTemplate(admin, 'palimpsest/pages/templates/admin.html')
join_ = admin._join; escape_ = admin._escape

# coding: utf-8
def base_end (prescriptfiles=0, js="", postscriptfiles=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'                </div>\n'])
    extend_([u'                <!--<footer class="wrapper">\n'])
    extend_([u'                        <p>&copy; 2012</p>\n'])
    extend_([u'                        <p></p>\n'])
    extend_([u'                </footer>-->\n'])
    extend_([u'\n'])
    extend_([u'        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>\n'])
    extend_([u'        <script>window.jQuery || document.write(\'<script src="/js/libs/jquery-1.7.2.min.js"><\\/script>\')</script>\n'])
    extend_([u'        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js"></script>\n'])
    extend_([u'        <!-- Load ScrollTo --><script type="text/javascript" src="http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js"></script>\n'])
    extend_([u'    <script src="/js/libs/selectToUISlider.jQuery.js"></script>\n'])
    extend_([u'\n'])
    if prescriptfiles != 0:
        for file in loop.setup(prescriptfiles):
            extend_(['            ', u'    <script src="', escape_(file, True), u'"></script>\n'])
            extend_(['            ', u'\n'])
    extend_([u'        <script>\n'])
    extend_([u'          ', escape_(js, False), u'\n'])
    extend_([u'        </script>\n'])
    extend_([u'\n'])
    if postscriptfiles != 0:
        for file in loop.setup(postscriptfiles):
            extend_(['            ', u'    <script src="', escape_(file, True), u'"></script>\n'])
            extend_(['            ', u'\n'])
    extend_([u'        <!--<script>\n'])
    extend_([u"                var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];\n"])
    extend_([u'                (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n'])
    extend_([u"                g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';\n"])
    extend_([u"                s.parentNode.insertBefore(g,s)}(document,'script'));\n"])
    extend_([u'</script>-->\n'])
    extend_([u'        </body>\n'])
    extend_([u'</html>\n'])

    return self

base_end = CompiledTemplate(base_end, 'palimpsest/pages/templates/base_end.html')
join_ = base_end._join; escape_ = base_end._escape

# coding: utf-8
def base_nav_narrow (left, right):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'    <a href="/" title="Palimpsest Home">\n'])
    extend_([u'        <h1><img src="/img/logo_p.png" alt="Palimpsest Home" title="Home" /></h1>\n'])
    extend_([u'    </a>\n'])
    extend_([u'                <nav>\n'])
    extend_([u'                        <ul class="left">\n'])
    for li in loop.setup(left):
        extend_(['                                ', u'<li>', escape_(li, False), u'</li>\n'])
    extend_([u'                        </ul>\n'])
    extend_([u'      <ul class="right">\n'])
    for li in loop.setup(right):
        extend_(['          ', u'<li>', escape_(li, False), u'</li>\n'])
    extend_([u'          <li><a href="/you"><i title="Your account" class="icon-user"></i></a></li>\n'])
    extend_([u'          <li><a href="', escape_(logout_url(), True), u'"><i title="Logout" class="icon-off"></i></a></li>\n'])
    extend_([u'      </ul>\n'])
    extend_([u'                </nav>\n'])

    return self

base_nav_narrow = CompiledTemplate(base_nav_narrow, 'palimpsest/pages/templates/base_nav_narrow.html')
join_ = base_nav_narrow._join; escape_ = base_nav_narrow._escape

# coding: utf-8
def base_nav_user (extras):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    if cur_user():
        extend_([u'    <nav class="btnmenu clearfix" role="navigation">\n'])
        extend_([u'            <ul class="left">\n'])
        extend_([u'                    <li>\n'])
        extend_([u'                            <a href="/you">Hi ', escape_(print_cur_user(), True), u'<i class="icon-caret-down"></i></a>\n'])
        extend_([u'                            <ul>\n'])
        extend_([u'                                    <li><a href="/you"><i class="icon-user"></i> Account</a></li>\n'])
        extend_([u'                                    <li><a href="', escape_(logout_url(), True), u'"><i class="icon-off"></i> Logout</a></li>\n'])
        extend_([u'                            </ul>\n'])
        extend_([u'                    </li>\n'])
        extend_([u'            </ul>\n'])
        extend_([u'            <ul class="right">\n'])
        extend_([u'                    <li><a href="/yours/saves">Journeys</a></li>\n'])
        extend_([u'                    <li><a href="/yours">Creations</a></li>\n'])
        for extra in loop.setup(extras):
            extend_(['                    ', u'    <li><a href="', escape_(extra[1], True), u'">', escape_(extra[0], True), u'</a></li>\n'])
        extend_([u'            </ul>\n'])
        extend_([u'    </nav>\n'])

    return self

base_nav_user = CompiledTemplate(base_nav_user, 'palimpsest/pages/templates/base_nav_user.html')
join_ = base_nav_user._join; escape_ = base_nav_user._escape

# coding: utf-8
def base_top_big (title, main_nav, user_nav=None):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'<!doctype html>\n'])
    extend_([u'<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->\n'])
    extend_([u'<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->\n'])
    extend_([u'<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->\n'])
    extend_([u'<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->\n'])
    extend_([u'        <head>\n'])
    extend_([u'                <meta charset="utf-8" />\n'])
    extend_([u'                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n'])
    extend_([u'\n'])
    extend_([u'                <title>', escape_(title, True), u'</title>\n'])
    extend_([u'\n'])
    extend_([u'                <meta name="description" content="" />\n'])
    extend_([u'                <meta name="author" content="" />\n'])
    extend_([u'\n'])
    extend_([u'                <meta name="viewport" content="width=device-width" />\n'])
    extend_([u'\n'])
    extend_([u'                <link rel="stylesheet" href="/css/style.css" />\n'])
    extend_([u'                <!--[if lte IE 8]> \n'])
    extend_([u'                        <script src="js/libs/modernizr-2.5.3-respond-1.1.0.min.js"></script> \n'])
    extend_([u'                        <link rel="stylesheet" href="/css/font-awesome-ie7.css" />\n'])
    extend_([u'                <![endif]-->\n'])
    extend_([u'\n'])
    extend_([u'        </head>\n'])
    extend_([u'        <body>\n'])
    extend_([u'                <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->\n'])
    extend_([u'                        <header class="clearfix">\n'])
    extend_([u'                                ', escape_(main_nav, False), u'\n'])
    extend_([u'                                <a href="/" title="Palimpsest home"><h1>Palimpsest</h1></a>\n'])
    extend_([u'                        </header>\n'])
    extend_([u'                <div class="content clearfix">\n'])
    extend_([u'                        <div class="wrapper">\n'])
    extend_([u'                                <noscript>\n'])
    extend_([u"                                        <p>You'll need JavaScript enabled to actually use the app.</p>\n"])
    extend_([u'                                </noscript>\n'])
    extend_([u'                                <div class="inner clearfix">\n'])
    extend_([u'                                        ', escape_(user_nav, False), u'\n'])
    extend_([u'                                </div>\n'])
    extend_([u'                        </div>\n'])

    return self

base_top_big = CompiledTemplate(base_top_big, 'palimpsest/pages/templates/base_top_big.html')
join_ = base_top_big._join; escape_ = base_top_big._escape

# coding: utf-8
def base_top_small (title, nav):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'<!doctype html>\n'])
    extend_([u'<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->\n'])
    extend_([u'<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->\n'])
    extend_([u'<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->\n'])
    extend_([u'<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->\n'])
    extend_([u'        <head>\n'])
    extend_([u'                <meta charset="utf-8" />\n'])
    extend_([u'                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />\n'])
    extend_([u'\n'])
    extend_([u'                <title>', escape_(title, True), u'</title>\n'])
    extend_([u'\n'])
    extend_([u'                <meta name="description" content="" />\n'])
    extend_([u'                <meta name="author" content="" />\n'])
    extend_([u'\n'])
    extend_([u'                <meta name="viewport" content="width=device-width" />\n'])
    extend_([u'\n'])
    extend_([u'                <link rel="stylesheet" href="/css/style.css" />\n'])
    extend_([u'                <!--[if lte IE 8]> \n'])
    extend_([u'                        <script src="js/libs/modernizr-2.5.3-respond-1.1.0.min.js"></script> \n'])
    extend_([u'                        <link rel="stylesheet" href="/css/font-awesome-ie7.css" />\n'])
    extend_([u'                <![endif]-->\n'])
    extend_([u'\n'])
    extend_([u'        </head>\n'])
    extend_([u'        <body>\n'])
    extend_([u'                <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->\n'])
    extend_([u'                        <header class="bar clearfix">\n'])
    extend_([u'                                ', escape_(nav, False), u'\n'])
    extend_([u'                        </header>\n'])
    extend_([u'                <div class="content clearfix">\n'])
    extend_([u'                        <div class="wrapper">\n'])
    extend_([u'                                <noscript>\n'])
    extend_([u"                                        <p>You'll need JavaScript enabled to actually use the app.</p>\n"])
    extend_([u'                                </noscript>\n'])
    extend_([u'                        </div>\n'])

    return self

base_top_small = CompiledTemplate(base_top_small, 'palimpsest/pages/templates/base_top_small.html')
join_ = base_top_small._join; escape_ = base_top_small._escape

# coding: utf-8
def cover (world, main_nav={"main":("Home", "/")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title="%s on Palimpsest" % world.title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'                <div class="wrapper">\n'])
    extend_([u'                        <div class="w1of2">\n'])
    extend_([u'                                <div class="inner clearfix">\n'])
    extend_([u'                                        <h2>', escape_(world.title, True), u'</h2>\n'])
    extend_([u'                                        <p class="align-left">', escape_(world.blurb, True), u'</p>\n'])
    extend_([u'                    <p class="color2">The pink shapes on the map represent areas where you\'ll be able to find texts.</p>\n'])
    extend_([u'                    <div id="map-canvas" style="height:300px;width:100%;"></div>\n'])
    extend_([u'                                </div>\n'])
    extend_([u'                        </div>\n'])
    extend_([u'            <div class="w1of2">\n'])
    extend_([u'                <div class="inner align-left">\n'])
    extend_([u'                    <h3>Out and about?</h3>\n'])
    extend_([u"                    <p>Use this button if you're ready to walk around the city with your smartphone or tablet in hand.</p>\n"])
    if cur_user():
        extend_(['                    ', u'<form method="post" action="/', escape_(world.key.id(), True), u'">\n'])
        extend_(['                    ', u'    <p>\n'])
        extend_(['                    ', u'        <label for="savename">If you\'d like to save your journey, enter a name to save it under so you can find it agian later. Leave it blank if you don\'t want to save.</label>\n'])
        extend_(['                    ', u'        <input type="text" name="savename" id="savename" />\n'])
        extend_(['                    ', u'    </p>\n'])
        extend_(['                    ', u'    <p><input type="submit" name="saveandgo" class="btnsq color2-gradient lighter-sh" value="Start" /></p>\n'])
        extend_(['                    ', u"    <h3>Can't get out?</h3>\n"])
        extend_(['                    ', u"    <p>If you're not in Edinburgh, can't leave your flat, can't share your location, or the weather is just awful, fear not!  You can use the armchair version here:</p>\n"])
        extend_(['                    ', u'    <p>(You can still save your journey by entering a name in the box above).</p>\n'])
        extend_(['                    ', u'    <p><input type="submit" name="sofago" class="btnsq color2-gradient lighter-sh" value="Start" /></p>\n'])
        extend_(['                    ', u'</form>\n'])
    else:
        extend_(['                    ', u'<p>If you <a href="', escape_(login_url(), True), u'" class="color1">sign in</a>, you can save your journey and share it with others.</p>\n'])
        extend_(['                    ', u'<form method="post" action="/', escape_(world.key.id(), True), u'">\n'])
        extend_(['                    ', u'    <input type="hidden" name="savename" id="savename" value="" />\n'])
        extend_(['                    ', u'    <p><input type="submit" name="saveandgo" class="btnsq color2-gradient lighter-sh" value="Start" /></p>\n'])
        extend_(['                    ', u"    <h3>Can't get out?</h3>\n"])
        extend_(['                    ', u"    <p>If you're not in Edinburgh, can't leave your flat, can't share your location, or the weather is just awful, fear not!  You can use the armchair version here:</p>\n"])
        extend_(['                    ', u'    <p><input type="submit" name="sofago" class="btnsq color2-gradient lighter-sh" value="Start" /></p>\n'])
        extend_(['                    ', u'</form>\n'])
    extend_([u'                </div>\n'])
    extend_([u'            </div>\n'])
    extend_([u'                </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

cover = CompiledTemplate(cover, 'palimpsest/pages/templates/cover.html')
join_ = cover._join; escape_ = cover._escape

# coding: utf-8
def feedback (form, fbs=None, message=None, title="Leave feedback about Palimpsest", main_nav={"main":("Home", "/")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title=title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'                <div class="wrapper">\n'])
    extend_([u'                        <div class="w1of1">\n'])
    extend_([u'                                <div class="inner align-left clearfix">\n'])
    if fbs is not None:
        extend_(['                                        ', u'    <h3>Feedback left</h3>\n'])
        for fb in loop.setup(fbs):
            if not fb.deleting:
                extend_(['                                                ', u'    <p>\n'])
                extend_(['                                                ', u'            <strong>', escape_(fb.date, True), u', \n'])
                if fb.user:
                    extend_(['                                                                    ', u'    ', escape_(fb.user.printable(), True), u'\n'])
                    if fb.user.contact:
                        extend_(['                                                                        ', u'     (may contact)\n'])
                    else:
                        extend_(['                                                                        ', u'     (may not contact)\n'])
                else:
                    extend_(['                                                                    ', u'    Anon\n'])
                extend_(['                                                ', u'             said:</strong>\n'])
                extend_(['                                                ', u'    </p>\n'])
                extend_(['                                                ', u'    <p>', escape_(fb.general, True), u'</p>\n'])
            else:
                extend_(['                                                ', u'    <p><strong>', escape_(fb.date, True), u', Account deleted message:</strong></p>\n'])
                extend_(['                                                ', u'    <p>', escape_(fb.deleting, True), u'</p>\n'])
        extend_(['                                        ', u'    <hr/>\n'])
    if message is not None:
        extend_(['                                        ', u'    <p class="win">', escape_(message, True), u'</p>\n'])
    extend_([u'                                        <h3>Send us your feedback</h3>\n'])
    extend_([u'                                        <p>We welcome any and all comments about Palimpsest.</p>\n'])
    extend_([u'                                        <p>Technical feedback can include any problems you might have had with the functioning of the app, errors you might have encountered, etc.  It would be helpful to us if you could mention the type and model of device you use to access Palimpsest, and the web browser you use.</p>\n'])
    if cur_user():
        extend_(['                                        ', u'    <p>If you change your mind about allowing us to contact you about your experiences with Palimpsest, you can change this setting on <a href="/you" class="color1">your account page</a>.</p>\n'])
    else:
        extend_(['                                        ', u'    <p>If you want to be contacted about your feedback, please <a href="', escape_(login_url, True), u'" class="color1">sign in</a> before submitting the form.</p>\n'])
    extend_([u'                                        ', escape_(form.render(), False), u'\n'])
    extend_([u'                                </div>\n'])
    extend_([u'                        </div>\n'])
    extend_([u'                </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

feedback = CompiledTemplate(feedback, 'palimpsest/pages/templates/feedback.html')
join_ = feedback._join; escape_ = feedback._escape

# coding: utf-8
def go (title="Palimpsest", navleft=[('Home','/')], navright=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_small(navleft=navleft, navright=navright, title=title), False), u'\n'])
    extend_([u'        <div class="wrapper">\n'])
    extend_([u'            <!--<p id="longbtn">Add long text</p>\n'])
    extend_([u'            <p id="shortbtn">Add short text</p>-->\n'])
    extend_([u'            <!--<form method="post"><input type="text" name="key" value="7" /><input type="submit" name="test" value="test" /></form>-->\n'])
    extend_([u'            <div id="sofa-map"></div>\n'])
    extend_([u'            <div id="page">\n'])
    extend_([u'\n'])
    extend_([u'                <div id="page-scroll">\n'])
    extend_([u'                    <p id="up" class="tabsq color1">\n'])
    extend_([u'                        <i class="icon-circle-arrow-up"></i>\n'])
    extend_([u'                    </p>\n'])
    extend_([u'                    <p id="down" class="tabsq color1">\n'])
    extend_([u'                        <i class="icon-circle-arrow-down"></i>\n'])
    extend_([u'                    </p>\n'])
    extend_([u'                </div>\n'])
    extend_([u'                <div id="page-side">\n'])
    extend_([u'                    <p id="getinfo" class="tabsq color1-bg" title="More information about current text">\n'])
    extend_([u'                        <i class="icon-info-sign"></i>\n'])
    extend_([u'                    </p>\n'])
    extend_([u'                    <p id="loadnext" title="Number of queued texts" class="tabsq color2-bg"><span id="no">0</span></p>\n'])
    extend_([u'                    <p id="playpause" title="Turn text queuing on or off" class="tabsq color1-bg color3">\n'])
    extend_([u'                        <i class="icon-play"></i>\n'])
    extend_([u'                    </p>\n'])
    extend_([u'                </div>\n'])
    extend_([u'\n'])
    extend_([u'                <p id="loading"><i class="icon-magic"></i> Please wait, we\'re looking for texts... If nothing happens, please make sure you have JavaScript enabled, GPS turned on and have shared your location with us.</p>\n'])
    extend_([u'                \n'])
    extend_([u'                <div id="info"><div class="inner"></div></div>\n'])
    extend_([u'                <div class="inner">\n'])
    extend_([u'                </div>\n'])
    extend_([u'            </div>\n'])
    extend_([u'            \n'])
    extend_([u'        </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

go = CompiledTemplate(go, 'palimpsest/pages/templates/go.html')
join_ = go._join; escape_ = go._escape

# coding: utf-8
def index (title="Palimpsest", main_nav={"main":("About", "#about")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title=title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'                <div class="wrapper">\n'])
    extend_([u'                        <div class="w1of2">\n'])
    extend_([u'                                <div class="inner clearfix">\n'])
    extend_([u'                                        <h2>Literary Edinburgh</h2>\n'])
    if cur_user():
        extend_(['                                        ', u'    <a href="', escape_(login_url(), True), u'">\n'])
        extend_(['                                        ', u'            <div class="w1of2-2 lighter-hov clearfix">\n'])
        extend_(['                                        ', u'                    <p><img src="/img/google.png" alt="Google" /></p>\n'])
        extend_(['                                        ', u'                    <p class="left btnbeside">You are signed in with Google</p>\n'])
        extend_(['                                        ', u'            </div>\n'])
        extend_(['                                        ', u'    </a>\n'])
        extend_(['                                        ', u'    <a href="/literary-edinburgh">\n'])
        extend_(['                                        ', u'            <div class="w1of2-2 lighter-hov clearfix">\n'])
        extend_(['                                        ', u'                    <p class="btnsq color2-gradient lighter-sh">start <i class="icon-caret-right"></i></p>\n'])
        extend_(['                                        ', u'                    <p class="right btnbeside">Your journey can be saved</p>\n'])
        extend_(['                                        ', u'            </div>\n'])
        extend_(['                                        ', u'    </a>\n'])
    else:
        extend_(['                                        ', u'    <a href="', escape_(login_url(), True), u'">\n'])
        extend_(['                                        ', u'            <div class="w1of2-2 lighter-hov clearfix">\n'])
        extend_(['                                        ', u'                    <p><img src="/img/google.png" alt="Google" /></p>\n'])
        extend_(['                                        ', u'                    <p class="left btnbeside">Sign in with Google to save your journey</p>\n'])
        extend_(['                                        ', u'            </div>\n'])
        extend_(['                                        ', u'    </a>\n'])
        extend_(['                                        ', u'    <a href="/literary-edinburgh">\n'])
        extend_(['                                        ', u'            <div class="w1of2-2 lighter-hov clearfix">\n'])
        extend_(['                                        ', u'                    <p class="btnsq color2-gradient lighter-sh">start <i class="icon-caret-right"></i></p>\n'])
        extend_(['                                        ', u'                    <p class="right btnbeside">Or continue without signing in</p>\n'])
        extend_(['                                        ', u'            </div>\n'])
        extend_(['                                        ', u'    </a>\n'])
    extend_([u'                                </div>\n'])
    extend_([u'                        </div>\n'])
    extend_([u'\n'])
    extend_([u'                        <div class="w1of2" id="about"><div class="inner clearfix">\n'])
    extend_([u'                                <h3>About</h3>\n'])
    extend_([u'                                <div class="align-left">\n'])
    extend_([u'                                        <p>\'<em>Palimpsest</em>: Literary Edinburgh\' originally arose out of the idea of creating an innovative new way of engaging people with literature to celebrate the 250th anniversary of the University of Edinburgh\'s English Literature department, the oldest in the world. <a href="/about" class="color1">Read more <i class="icon-caret-right"></i></a></p>\n'])
    extend_([u"                                        <p>To use, simply open the site in your smartphone or tablet's browser and hit 'start'.  Once you've set your preferences and clicked 'allow' when asked to share your location, you can walk around Edinburgh and see what you discover.</p>\n"])
    extend_([u'                                        <p><strong class="color2">New:</strong> Use Palimpsest from the comfort of your own home!  Start a journey to see how.</p>\n'])
    extend_([u'                                        <p class="color2"><strong>Coming soon:</strong> Contribute texts to <em>Literary Edinburgh</em>, or create your own experiences, anywhere you like! Watch this space.</p>\n'])
    extend_([u'                                        <p class="color1"><a href="/feedback">Have questions, comments, suggestions, or any other feedback for us? Click here <i class="icon-caret-right"></i></a></p>\n'])
    extend_([u'                                </div>\n'])
    extend_([u'                        </div></div>\n'])
    extend_([u'                </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

index = CompiledTemplate(index, 'palimpsest/pages/templates/index.html')
join_ = index._join; escape_ = index._escape

# coding: utf-8
def msg_page (title="Palimpsest", msg="This page works..", main_nav={"main":("Home", "/")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title=title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'                <div class="wrapper">\n'])
    extend_([u'                        <div class="w2of3">\n'])
    extend_([u'                                <div class="inner align-left clearfix">\n'])
    extend_([u'                                        <h2>', escape_(title, True), u'</h2>\n'])
    extend_([u'                                        <p>', escape_(msg, True), u'</p></blockquote>\n'])
    extend_([u'                                </div>\n'])
    extend_([u'                        </div>\n'])
    extend_([u'                </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

msg_page = CompiledTemplate(msg_page, 'palimpsest/pages/templates/msg_page.html')
join_ = msg_page._join; escape_ = msg_page._escape

# coding: utf-8
def nav (ul):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'                <nav class="btnmenu mob">\n'])
    extend_([u'                        <ul>\n'])
    for li in loop.setup(ul):
        extend_(['                                ', u'<li>', escape_(li, False), u'</li>\n'])
    extend_([u'                        </ul>\n'])
    extend_([u'                </nav>\n'])

    return self

nav = CompiledTemplate(nav, 'palimpsest/pages/templates/nav.html')
join_ = nav._join; escape_ = nav._escape

# coding: utf-8
def save (save, rooms, access=False, form=None, title="Palimpsest", main_nav={"main":("Home", "/")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title=title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'        <div class="wrapper">\n'])
    extend_([u'            <div class="w1of3">\n'])
    extend_([u'                <div class="inner align-left clearfix"><div class="color1-border inner">\n'])
    extend_([u'                    <h2>', escape_(save.name, True), u'</h2>\n'])
    extend_([u'                    <p>Started: ', escape_(date(save.date_added), True), u'</p>\n'])
    extend_([u'                    <p>Updated: ', escape_(date(save.date_modified), True), u'</p>\n'])
    extend_([u'\n'])
    if save.publish and not access:
        extend_(['                    ', u'<p><a class="btn lighter-bg light-border color1" href="https://twitter.com/intent/tweet?original_referer=http%3A%2F%2Fwww.literarycities.com&text=I+found+this+journey+with+Palimpsest%3A+Literary+Edinburgh%21+http%3A%2F%2Fliterarycities.org/journey/', escape_(save.key.id(), True), u'+%23palimpsest" target="_blank">\n'])
        extend_(['                    ', u'    <i class="icon-twitter"></i> Tweet about this journey.\n'])
        extend_(['                    ', u'</a>\n'])
        extend_(['                    ', u'</p>\n'])
        extend_(['                    ', u'\n'])
    if access:
        extend_(['                    ', u'\n'])
        if save.publish:
            extend_(['                        ', u'<p><a class="btn lighter-bg light-border color1" href="https://twitter.com/intent/tweet?original_referer=http%3A%2F%2Fwww.literarycities.com&text=I+saved+a+journey+with+Palimpsest%3A+Literary+Edinburgh%21+http%3A%2F%2Fliterarycities.org/journey/', escape_(save.key.id(), True), u'+%23palimpsest" target="_blank">\n'])
            extend_(['                        ', u'    <i class="icon-twitter"></i> Tweet about this journey.\n'])
            extend_(['                        ', u'</a>\n'])
            extend_(['                        ', u'</p>\n'])
            extend_(['                        ', u'\n'])
        extend_(['                    ', u'    <form method="post">\n'])
        extend_(['                    ', u'        <p>\n'])
        extend_(['                    ', u'            <input type="checkbox" value="True" name="pub" id="pub"\n'])
        if save.publish:
            extend_(['                                ', u'checked\n'])
        extend_(['                    ', u'             />\n'])
        extend_(['                    ', u'             <label for="pub">Tick to make this journey public, so you can share the URL with others.</label>\n'])
        extend_(['                    ', u'        </p>\n'])
        extend_(['                    ', u'        <p><input type="submit" name="pubsub" value="Save" /></p>\n'])
        extend_(['                    ', u'    </form>\n'])
    extend_([u'                </div></div>\n'])
    extend_([u'            </div>\n'])
    extend_([u'            <div class="w2of3"><div class="inner align-left">\n'])
    for room in loop.setup(rooms):
        extend_(['                ', u'<h3>', escape_(room.name, True), u'</h3>\n'])
        extend_(['                ', u'<p style="border-bottom: 1px solid white; padding-bottom: 2em">', escape_(room.long_desc, True), u'</p>\n'])
    extend_([u'            </div></div>\n'])
    extend_([u'\n'])
    extend_([u'        </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

save = CompiledTemplate(save, 'palimpsest/pages/templates/save.html')
join_ = save._join; escape_ = save._escape

# coding: utf-8
def saves (saves, title="Palimpsest", main_nav={"main":("Home", "/")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title=title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'        <div class="wrapper">\n'])
    extend_([u'            <div class="w2of3">\n'])
    extend_([u'                <div class="inner align-left clearfix">\n'])
    extend_([u'                    <h2>Your saved journeys</h2>\n'])
    if len(saves) > 0:
        extend_(['                    ', u'<p>Click on your saved journeys to view and share them.</p>\n'])
        extend_(['                    ', u'<ul>\n'])
        for save in loop.setup(saves):
            extend_(['                    ', u'<li class="lighter-hov"><a href="/journey/', escape_(save.key.id(), True), u'">\n'])
            extend_(['                    ', u'    <strong>', escape_(save.name, True), u'</strong> on ', escape_(date(save.date_added), True), u'\n'])
            if save.publish:
                extend_(['                        ', u'(public)\n'])
            extend_(['                    ', u'</a></li>\n'])
        extend_(['                    ', u'</ul>\n'])
    else:
        extend_(['                    ', u'<p><em>You haven\'t saved any journeys yet. Try <a href="/literary-edinburgh" class="color1">Literary Edinburgh</a>.</em></p>\n'])
    extend_([u'                </div>\n'])
    extend_([u'            </div>\n'])
    extend_([u'\n'])
    extend_([u'        </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

saves = CompiledTemplate(saves, 'palimpsest/pages/templates/saves.html')
join_ = saves._join; escape_ = saves._escape

# coding: utf-8
def section_moderators (form, mods=None):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'\n'])
    extend_([u'<h2>Invite moderators</h2>\n'])
    extend_([u'<p>Moderators can submit texts and properties that are published straight away, as well as edit existing texts and properties.</p>\n'])
    extend_([u'<p>If this experience accepts contributions from the public, moderators can accept or reject them.</p>\n'])
    extend_([u'<p>Promoting a moderator to admin will additionally allow them to edit the experience settings, and modify the access rights of other users.</p>\n'])
    extend_([u"<p>Invite someone to moderate by typing in their email address.  They'll recieve an email to let them know you've invited them.  If the person you invite doesn't have a Google account associated with that email address, they'll be asked to create one so they can log into the site. </p>\n"])
    extend_([escape_(form.render(), False), u'\n'])
    extend_([u'\n'])
    extend_([u'<h2>Existing moderators</h2>\n'])
    if mods is not None and len(mods) > 0:
        for wc in loop.setup(mods):
            extend_(['    ', u'    <div class="w1of1 lighter-hov clearfix"><p style="display: inline-block; min-width: 50%">\n'])
            if wc.user is not None:
                extend_(['        ', u'    ', escape_(wc.user.get().printable(), True), u'\n'])
            else:
                extend_(['        ', u'    ', escape_(wc.user_pending.get().email, True), u' (pending sign-up)\n'])
            extend_(['    ', u'    </p>\n'])
            extend_(['    ', u'    <form method="post" style="display: inline-block; min-width:40%">\n'])
            extend_(['    ', u'            <input type="hidden" value="', escape_(wc.key.id(), True), u'" name="userid" id="userid" />\n'])
            extend_(['    ', u'            <select name="action" style="min-width: 30%">\n'])
            extend_(['    ', u'                    <option value="0">Remove</option>\n'])
            if wc.admin:
                extend_(['                        ', u'    <option value="2">Revoke admin</option>\n'])
            else:
                extend_(['                        ', u'    <option value="1">Promote to admin</opion>\n'])
            extend_(['    ', u'            </select>\n'])
            extend_(['    ', u'            <input type="submit" name="mod" value="Update access" />\n'])
            extend_(['    ', u'    </form>\n'])
            extend_(['    ', u'    </div>\n'])
    else:
        extend_([u'    <p>Only you.</p>\n'])

    return self

section_moderators = CompiledTemplate(section_moderators, 'palimpsest/pages/templates/section_moderators.html')
join_ = section_moderators._join; escape_ = section_moderators._escape

# coding: utf-8
def section_property (form, del_form=None, edit=False):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'\n'])
    if edit:
        extend_([u'    <h2>Edit property</h2>\n'])
        extend_([u'    <p>Delete this property (cannot be undone):</p>\n'])
        extend_([u'    ', escape_(del_form.render(), False), u'\n'])
        extend_([u'    <p>Or change the settings for this property here.</p>\n'])
    else:
        extend_([u'    <h2>Create a new property</h2>\n'])
        extend_([u'    <p>This is where you set up the properties that are available for <strong>all texts</strong> in the experience.</p>\n'])
        extend_([u'    <p>Properties give the reader either more control over, or more information about (or a combination of the two) the texts in the experience. You can add <em>values</em> for these properties for each individual text when you add or edit that text.</p>\n'])
    extend_([escape_(form.render(), False), u'\n'])

    return self

section_property = CompiledTemplate(section_property, 'palimpsest/pages/templates/section_property.html')
join_ = section_property._join; escape_ = section_property._escape

# coding: utf-8
def section_property_read (property):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'\n'])
    extend_([u'        <h2>', escape_(property.name, True), u'</h2>\n'])
    extend_([u'        <p>Used for: <strong>\n'])
    if property.sorter:
        extend_(['                ', u'    filtering texts\n'])
        if property.discrete:
            extend_(['                    ', u'     (with checkboxes)\n'])
        else:
            extend_(['                    ', u'     (on a sliding scale)\n'])
    if property.sorter and property.info:
        extend_(['                ', u'     and \n'])
    if property.info:
        extend_(['                ', u'    extra information about texts\n'])
    if not (property.info or property.sorter):
        extend_(['                ', u'     system information about text\n'])
    extend_([u'        </strong></p>\n'])
    extend_([u'        <p>Valid values: <strong>\n'])
    if property.valid_values == "|num|":
        extend_(['                ', u'    Numeric\n'])
    elif property.valid_values == "|text|":
        extend_(['                ', u'    Text\n'])
    else:
        extend_(['                ', u'    ', escape_(property.valid_values, True), u'\n'])
    extend_([u'        </strong></p>\n'])

    return self

section_property_read = CompiledTemplate(section_property_read, 'palimpsest/pages/templates/section_property_read.html')
join_ = section_property_read._join; escape_ = section_property_read._escape

# coding: utf-8
def section_texts (form, properties_available, del_form=None, edit=False, properties=[]):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'\n'])
    if edit:
        extend_([u'<h2>Edit text</h2>\n'])
        extend_([u'<p>Delete this text and associated property values (cannot be undone):</p>\n'])
        extend_([escape_(del_form.render(), False), u'\n'])
        extend_([u'<p>Or change the details this text here.</p>\n'])
    else:
        extend_([u'<h2>Add a new text</h2>\n'])
        extend_([u'<p>Texts are the core of an experience!</p>\n'])
        extend_([u'\n'])
    extend_([u'<form id="roomcreate" method="post">\n'])
    extend_([u'    ', escape_(form.render(), False), u'\n'])
    extend_([u'    \n'])
    extend_([u'    <p><strong>Properties</strong></p>\n'])
    if properties_available:
        extend_(['    ', u'<p class="btn lighter-bg color1-border color4-bg-hov" id="propertyBtn" style="cursor: pointer;"> + Add text properties</p>\n'])
        extend_(['    ', u'<div id="propertyBtns"></div>\n'])
        extend_(['    ', u'<div id="validValues" style="display:none;visibility:hidden;"></div>\n'])
        extend_(['    ', u'\n'])
        if edit:
            if len(properties['visible']) > 0:
                extend_(['    ', u'<p>Tick the box next to properties you want to <strong>delete</strong>.  To modify a property, tick the box beside the old one to delete it, and create a new value using the button above.</p>\n'])
                for p in loop.setup(properties['visible']):
                    extend_(['    ', u'<p>\n'])
                    extend_(['    ', u'    <input type="checkbox" name="del_property" value="', escape_(p.key.id(), True), u'" class="del_property" />\n'])
                    extend_(['    ', u'    <span><em>', escape_(p.of_property.get().name, True), u'</em>: ', escape_(p.value, True), u'</span>\n'])
                    extend_(['    ', u'</p>\n'])
            if len(properties['pending']) > 0:
                extend_(['    ', u'<p><strong>Properties pending moderation</strong></p>\n'])
                for p in loop.setup(properties['pending']):
                    extend_(['    ', u'<p>\n'])
                    extend_(['    ', u'    <select name="action_val" class="action_val" />\n'])
                    extend_(['    ', u'            <option value="0">No action</option>\n'])
                    extend_(['    ', u'            <option value="2">Accept</option>\n'])
                    extend_(['    ', u'            <option value="1">Reject (and delete)</option>\n'])
                    extend_(['    ', u'    </select>\n'])
                    extend_(['    ', u'    <input type="hidden" value="', escape_(p.key.id(), True), u'" name="mod_id" id="mod_id" />\n'])
                    extend_(['    ', u'    <span><em>', escape_(p.of_property.get().name, True), u'</em>: ', escape_(p.value, True), u'</span>\n'])
                    extend_(['    ', u'</p>\n'])
                    extend_(['    ', u'\n'])
    else:
        extend_(['    ', u'<p class="fail">There aren\'t any properties available</p>\n'])
        extend_(['    ', u'\n'])
    extend_([u'    <p><strong>Text location</strong></p>\n'])
    if edit:
        extend_(['    ', u"<p>Adjust a text's position by moving the corners of the blue shape. Delete points by right clicking on them.</p>\n"])
    else:
        extend_(['    ', u'<p class="serif">Single-click points on the map that are the corners of the text location. As you click, the points will join up to form a shape.  Move the points around to refine the area.</p>\n'])
    extend_([u'    <div id="coordInputs"></div>\n'])
    extend_([u'    <div id="map-canvas"></div>\n'])
    extend_([u'    \n'])
    extend_([u'    <p><input type="submit" name="save_text" id="save_text" value="Save" /></p>\n'])
    extend_([u'</form>\n'])

    return self

section_texts = CompiledTemplate(section_texts, 'palimpsest/pages/templates/section_texts.html')
join_ = section_texts._join; escape_ = section_texts._escape

# coding: utf-8
def section_texts_read (text, open, properties_available, property_values={}):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'\n'])
    extend_([u'<h2>', escape_(text.name, True), u'</h2>\n'])
    extend_([u'<p>Short version: <strong>', escape_(text.short_desc, True), u'</strong></p>\n'])
    extend_([u'<p>Long version: <strong>', escape_(text.long_desc, True), u'</strong></p>\n'])
    extend_([u'    \n'])
    extend_([u'    <p>Properties</p>\n'])
    extend_([u'    <ul class="normal">\n'])
    for pv in loop.setup(property_values):
        extend_(['    ', u'<li><strong>', escape_(pv.of_property.get().name, True), u': </strong>', escape_(pv.value, True), u' (added by ', escape_(pv.added_by.get().printable(), True), u')</li>\n'])
    extend_([u'    </ul>\n'])
    extend_([u'\n'])
    if open and properties_available:
        extend_(['    ', u'<p>If you have more information to add about this text, do so here.</p>\n'])
        extend_(['    ', u'<form id="properties" method="post">\n'])
        extend_(['    ', u'    <p class="btn lighter-bg color1-border color4-bg-hov" id="propertyBtn" style="cursor: pointer;"> + Add text properties</p>\n'])
        extend_(['    ', u'    <div id="propertyBtns"></div>\n'])
        extend_(['    ', u'    <div id="validValues" style="display:none;visibility:hidden;"></div>\n'])
        extend_(['    ', u'\n'])
        extend_(['    ', u'    <p><input type="submit" name="save_text" id="save_text" value="Save" /></p>\n'])
        extend_(['    ', u'</form>\n'])
        extend_(['    ', u'\n'])
    extend_([u'    <p>Text location:</p>\n'])
    extend_([u'    <div id="map-canvas"></div>\n'])

    return self

section_texts_read = CompiledTemplate(section_texts_read, 'palimpsest/pages/templates/section_texts_read.html')
join_ = section_texts_read._join; escape_ = section_texts_read._escape

# coding: utf-8
def section_world (form):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'\n'])
    extend_([u'<h2>Experience settings</h2>\n'])
    extend_([u"<p>Once you save a title for the overall experience you're creating, you can add texts and properties and more. You can come back and edit the other things here whenever you like.</p>\n"])
    extend_([escape_(form.render(), False), u'\n'])

    return self

section_world = CompiledTemplate(section_world, 'palimpsest/pages/templates/section_world.html')
join_ = section_world._join; escape_ = section_world._escape

# coding: utf-8
def user (form, del_form, message=None, title="Palimpsest", main_nav={"main":("Home", "/")}, user_nav=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_big(title=title, main_nav=main_nav, user_nav=user_nav), False), u'\n'])
    extend_([u'        <div class="wrapper">\n'])
    extend_([u'            <div class="w1of2">\n'])
    extend_([u'                <div class="inner align-left clearfix">\n'])
    if message is not None:
        extend_(['                    ', u'<p class="win">', escape_(message, True), u'</p>\n'])
    extend_([u'                    <h3>Your details</h3>\n'])
    extend_([u"                    <p>We'll use your penname beside experiences or texts you create, if they're listed anywhere.  If there's no penname, we'll use your real name, and without that, we'll use the nickname associated with your Google account (", escape_(cur_user().user.nickname(), True), u').</p>\n'])
    extend_([u"                    <p>Tick the box if you don't mind us contacting you for feedback about your use of Palimpsest.  The email address we currently have on record for you from your Google account is ", escape_(cur_user().user.email(), True), u'.</p>\n'])
    extend_([u'                    ', escape_(form.render(), False), u'\n'])
    extend_([u'                    <p class="color1"><a href="/literary/feedback">Have questions, comments, suggestions, or any other feedback for us? Click here &#187;</a></p>\n'])
    extend_([u'                </div>\n'])
    extend_([u'            </div>\n'])
    extend_([u'\n'])
    extend_([u'            <div class="w1of2"><div class="inner align-left clearfix">\n'])
    extend_([u'                <h3>Privacy</h3>\n'])
    extend_([u"                <p>You can rest assured we'll never give out any of your details to third parties.</p>\n"])
    extend_([u'                <p>Any feedback we recieve from you, or data we get from the journeys you save or experiences you create, will be used only to better understand usage patterns of the app and improve its implementation.  All data will remain anonymous unless you expressly grant us your permission to use it otherwise.</p>\n'])
    extend_([u'                <p>When you use the app, your location data is not recorded, simply used in the moment to detect nearby texts.</p>\n'])
    extend_([u'                <p>If you wish to delete your account, and all data associated with it (including experiences you have created and journeys you have saved), please use the form below.</p>\n'])
    extend_([u'                ', escape_(del_form.render(), False), u'\n'])
    extend_([u'            </div></div>\n'])
    extend_([u'\n'])
    extend_([u'        </div>\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

user = CompiledTemplate(user, 'palimpsest/pages/templates/user.html')
join_ = user._join; escape_ = user._escape

# coding: utf-8
def yours (menu, title="Palimpsest", navleft=[('Back home','/')], navright=[], pre=0, js="", post=0):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([escape_(top_small(navleft=navleft, navright=navright, title=title), False), u'\n'])
    extend_([u'\n'])
    extend_([u'        <div class="admin-head clearfix">\n'])
    extend_([u'            <h2>Your experiences</h2>\n'])
    extend_([u'        </div>\n'])
    extend_([u'\n'])
    extend_([u'        <div class="has-side"><div class="inner align-left">\n'])
    extend_([u'            <p>You can create your own experience or contrinbute texts to the open experiences listed.</p>\n'])
    extend_([u"            <p>If you've been invited to a closed experience, you can add texts to that as well.</p>\n"])
    extend_([u'        </div></div>\n'])
    extend_([u'\n'])
    extend_([u'        <div class="side" id="menu">\n'])
    extend_([u'            <nav>\n'])
    extend_([u'                <ul class="li1">\n'])
    extend_([u'                    <!-- new/yours -->\n'])
    if len(menu['yours']) > 0:
        extend_(['                    ', u'<li class="', escape_(menu['yours'][0][2], True), u'">\n'])
        extend_(['                    ', u'    <h3>\n'])
        extend_(['                    ', u'        <a class="clearfix" href="', escape_(menu['yours'][0][1], True), u'">\n'])
        extend_(['                    ', u'            <span class="left">', escape_(menu['yours'][0][0], True), u'</span><i class="right ', escape_(menu['yours'][0][3], True), u'"></i>\n'])
        extend_(['                    ', u'        </a>\n'])
        extend_(['                    ', u'    </h3>\n'])
        if len(menu['yours']) > 1:
            extend_(['                        ', u'<ul class="li2">\n'])
            for x in loop.setup(range(1,len(menu['yours']))):
                extend_(['                            ', u'<li class="', escape_(menu['yours'][x][2], True), u'">\n'])
                extend_(['                            ', u'    <a href="', escape_(menu['yours'][x][1], True), u'">', escape_(menu['yours'][x][0], True), u'\n'])
                extend_(['                            ', u'    <i class="icon-edit extra right"></i></a>\n'])
                extend_(['                            ', u'</li>\n'])
            extend_(['                        ', u'</ul>\n'])
        extend_(['                    ', u'</li>\n'])
    extend_([u'                    <!-- Closed -->\n'])
    if len(menu['unmod']) > 0:
        extend_(['                    ', u'<li class="', escape_(menu['unmod'][0][2], True), u'">\n'])
        extend_(['                    ', u'    <h3>\n'])
        extend_(['                    ', u'        <a class="clearfix" href="', escape_(menu['unmod'][0][1], True), u'">\n'])
        extend_(['                    ', u'            <span class="left">', escape_(menu['unmod'][0][0], True), u'</span><i class="right ', escape_(menu['unmod'][0][3], True), u'"></i>\n'])
        extend_(['                    ', u'        </a>\n'])
        extend_(['                    ', u'    </h3>\n'])
        if len(menu['unmod']) > 1:
            extend_(['                        ', u'<ul class="li2">\n'])
            for x in loop.setup(range(1,len(menu['unmod']))):
                extend_(['                            ', u'<li class="', escape_(menu['unmod'][x][2], True), u'">\n'])
                extend_(['                            ', u'    <a href="', escape_(menu['unmod'][x][1], True), u'">', escape_(menu['unmod'][x][0], True), u'\n'])
                extend_(['                            ', u'    <i class="icon-plus-sign extra right"></i></a>\n'])
                extend_(['                            ', u'</li>\n'])
            extend_(['                        ', u'</ul>\n'])
        extend_(['                    ', u'</li>\n'])
    extend_([u'                    <!-- Open -->\n'])
    if len(menu['contr']) > 0:
        extend_(['                    ', u'<li class="', escape_(menu['contr'][0][2], True), u'">\n'])
        extend_(['                    ', u'    <h3>\n'])
        extend_(['                    ', u'        <a class="clearfix" href="', escape_(menu['contr'][0][1], True), u'">\n'])
        extend_(['                    ', u'            <span class="left">', escape_(menu['contr'][0][0], True), u'</span><i class="right ', escape_(menu['contr'][0][3], True), u'"></i>\n'])
        extend_(['                    ', u'        </a>\n'])
        extend_(['                    ', u'    </h3>\n'])
        if len(menu['contr']) > 1:
            extend_(['                        ', u'<ul class="li2">\n'])
            for x in loop.setup(range(1,len(menu['contr']))):
                extend_(['                            ', u'<li class="', escape_(menu['contr'][x][2], True), u'">\n'])
                extend_(['                            ', u'    <a href="', escape_(menu['contr'][x][1], True), u'">', escape_(menu['contr'][x][0], True), u'\n'])
                extend_(['                            ', u'    <i class="icon-plus-sign extra right"></i></a>\n'])
                extend_(['                            ', u'</li>\n'])
            extend_(['                        ', u'</ul>\n'])
        extend_(['                    ', u'</li>\n'])
    extend_([u'                </ul>\n'])
    extend_([u'            </nav>\n'])
    extend_([u'        </div>\n'])
    extend_([u'\n'])
    extend_([escape_(end(pre=pre,js=js,post=post), False), u'\n'])

    return self

yours = CompiledTemplate(yours, 'palimpsest/pages/templates/yours.html')
join_ = yours._join; escape_ = yours._escape

