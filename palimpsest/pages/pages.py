import web
from google.appengine.ext import ndb
from google.appengine.api import users

from palimpsest.pages.page import Page, render
from palimpsest.utils.form import Form, CombinedForm, Submit, display_input_warning, display_form_warning
from palimpsest.logins import ensure_user_exists
from palimpsest.models import Feedback

class Index(Page):
    def GET(self):
        return render().index()

class About(Page):
    def GET(self):
        return render().about()

class LeaveFeedback(Page): # This is called LeaveFeedback so it doesn't conflict with Feedback model

    fb_form = Form(
        web.form.Textarea('general', description="General feedback")
        ,web.form.Textarea('technical', description="Technical feedback")
        ,web.form.Checkbox('contact', value=True, description='May we contact you about your experience?')
        ,Submit('send_feedback', value="Send")
        , pre_input=display_input_warning
        , pre_form=display_form_warning
    )
    formargs = {'method':'post'}

    def GET(self):

        f = self.fb_form()
        form = CombinedForm([f], **self.formargs)
        fbs = None
        if self.page_user:
            f.fill({'contact': self.page_user.contact})
            #TODO:
            #if self.page_user.is_admin_of(primary_world()):
            #    fbs = Feedback.all().order('-date')
        return render().feedback(form, fbs)

    def POST(self):
        f = self.fb_form()
        ef = self.fb_form()
        
        if not f.validates():
            return render().feedback(form)
        else:
            new_fb = Feedback(technical=f.d.technical, general=f.d.general)

            if self.page_user:
                new_fb.user=self.page_user.key
                self.page_user.contact = f.d.contact
                user_ftr = self.page_user.put_async()
                ef.fill({'contact': self.page_user.contact})

            new_fb_ftr = new_fb.put_async()

            form = CombinedForm([ef], **self.formargs)
            message = "Thanks for your feedback!  If you require a response, we'll get back to you as soon as possible."
            
            page = render().feedback(form, None, message)

            if self.page_user:
                user_ftr.get_result()
            new_fb_ftr.get_result()

            return page