from google.appengine.ext import ndb
from google.appengine.api import users, memcache

import web

from models import User, UserPending

def user_exists(user):
    if user:
        uid = user.user_id()
        user_future = User.get_by_id_async(uid)
        return user_future.get_result()
    return False

def ensure_user_exists(user):
    """ 
    If the user doesn't exist in the database as a User (ie. first time login) create them. 
    Return user object. 
    """
    uid = str(user.user_id())
    theuser_ftr = User.get_or_insert_async(uid, user=user)
    return theuser_ftr.get_result()

def ensure_logged_in():
    user = users.get_current_user()
    if user:
        return ensure_user_exists(user)
    else:
        raise web.seeother(get_login_url())

def ensure_user_admin():
    user = ensure_logged_in()
    if users.is_current_user_admin():
        return user
    else:
        raise web.seeother("/")

def get_login_url():
    return users.create_login_url(web.ctx.get('path', '/'))

def get_logout_url():
    return users.create_logout_url('/')

def get_current_user():
    user = users.get_current_user()
    return user_exists(user)

def has_pending(user=None):
    if user is None:
        user = get_current_user()
    email = user.user.email().lower()
    u_p = UserPending.query(UserPending.email == email)
    if u_p.count(limit=1) > 0:
        return True
    else:
        return False

def get_user_pending(user=None):
    if user is None:
        user = get_current_user()
    email = user.user.email().lower()
    u_p = UserPending.query(UserPending.email == email)
    return u_p

"""def get_pending(user=None):
    if user is None:
        user = get_current_user()
    return get_user_pending(user).will_have_access"""


""" prefix is likely to be '_properties-' or '' (for menus) """
def has_cache(world, prefix, user=None):
    if user is None:
        user = get_current_user()
    data = memcache.get("%s%s-%s" % (prefix, world.key.id(), user.user.user_id()))
    if data is not None:
        return True
    return False

def get_users_with_cache(prefix, world):
    return memcache.get("%s%s-list" % (prefix, world.key.id()))

def add_user_to_cache_list(prefix, world, userid):
    existing = get_users_with_cache(prefix, world)
    if existing is None:
        userids = [userid]
    elif userid not in existing:
        userids = existing + [userid]
    else:
        userids = existing
    return memcache.set("%s%s-list" % (prefix, world.key.id()), userids)

def flush_caches(prefix, world, rpc):
    mc = memcache.Client()
    userids = get_users_with_cache(prefix, world)
    try:
        mc.delete_multi_async(userids, key_prefix="%s%s-" % (prefix, world.key.id()), rpc=rpc)
    except TypeError:
        pass
    memcache.delete("%s%s-list" % (prefix, world.key.id()))
    return rpc

