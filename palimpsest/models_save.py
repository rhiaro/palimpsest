from google.appengine.ext import ndb
from google.appengine.api import memcache
from models_room import Room
from models_user import User

class SavePath(ndb.Model):
    user = ndb.KeyProperty(kind=User)
    date_added = ndb.DateTimeProperty(auto_now_add=True)
    date_modified = ndb.DateTimeProperty(auto_now=True)
    texts = ndb.KeyProperty(kind=Room, repeated=True)
    name = ndb.StringProperty()
    publish = ndb.BooleanProperty()

def make_save_name(name, user, i=1):
    existing = SavePath.query(SavePath.name == name).fetch(keys_only = True)
    if existing != []:
        i += 1
        new_name = "%s %s" % (name, i)
        return make_save_name(new_name, user, i)
    
    return name

def get_saves_by(user):
    q = SavePath.query(SavePath.user == user.key)
    q = q.order(-SavePath.date_added)
    saves = q.fetch()
    return saves

def save_exists(kn):
    return SavePath.get_by_id(int(kn))