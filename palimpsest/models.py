from google.appengine.ext import ndb
from models_user import User, UserPending, get_google_user_key, user_exists_bykey
from models_save import SavePath, make_save_name, get_saves_by, save_exists
from models_world import World, get_default_void, world_exists, get_everything_by
from models_property import Property, PropertyValue, property_exists
from models_room import Room, text_exists
from models_moderator import Moderator

class Feedback(ndb.Model):
    user = ndb.KeyProperty(kind=User)
    date = ndb.DateTimeProperty(auto_now_add=True)
    technical = ndb.TextProperty()
    general = ndb.TextProperty()
    userid = ndb.StringProperty()
    deleting = ndb.TextProperty()