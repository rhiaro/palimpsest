from google.appengine.ext import ndb
from models_world import World
from models_user import User

class Room(ndb.Model):
    name = ndb.StringProperty()
    short_desc = ndb.TextProperty()
    long_desc = ndb.TextProperty()
    latitudes = ndb.StringProperty(repeated=True)
    longitudes = ndb.StringProperty(repeated=True)

    world = ndb.KeyProperty(kind=World) #rooms
    added_by = ndb.KeyProperty(kind=User) #contributions
    last_modified_by = ndb.KeyProperty(kind=User) #rooms_modified
    date_added = ndb.DateTimeProperty(auto_now_add=True)
    last_modified_date = ndb.DateTimeProperty(auto_now=True)

    visible = ndb.BooleanProperty() # If True for public submission, implicitly 'accepted'
    rejected = ndb.BooleanProperty() # If False for public submission, status is 'pending'
    rejected_from = ndb.KeyProperty(kind=World) #rejected_rooms

    pending = ndb.ComputedProperty(lambda self: self.is_pending())

    #pending_values = ndb.ComputedProperty(lambda self: self.has_pending_values())

    def is_pending(self):
        return self.visible == False and self.rejected == False

    def in_world(self, world):
        return self.world.key.string_id() == world.key.string_id()

    def is_made_by(self, u):
        return self.added_by == u.key

    def useable_by(self, user):
        return self.visible or (self.is_made_by(user) and not self.rejected)

    def changeable_by(self, user):
        """ Can edit/delete if mod OR (world open AND user made self AND not visible AND not rejected) """
        return self.world.is_mod(user) or (self.world.is_open and (not self.visible) and (not self.rejected) and self.added_by.user == user.key)

    def accepts_property_value_by(self, user):
        return self.changeable_by(user) or (self.world.is_open and self.visible)

    @ndb.tasklet
    def get_property_values(self):
        from models_property import PropertyValue
        values = yield PropertyValue.query(PropertyValue.room == self.key).fetch_async()
        yield ndb.get_multi_async([value.of_property for value in values])
        raise ndb.Return(values)

    @ndb.tasklet
    def get_allowed_property_values(self, user):
        from models_property import PropertyValue
        values = yield PropertyValue.query(ndb.AND(PropertyValue.room == self.key, ndb.OR(PropertyValue.visible == True, PropertyValue.added_by == user.key))).fetch_async()
        yield ndb.get_multi_async([value.of_property for value in values])
        raise ndb.Return(values)

    @ndb.tasklet
    def get_pending_property_values(self):
        from models_property import PropertyValue
        values = yield PropertyValue.query(PropertyValue.room == self.key, PropertyValue.visible == False).fetch_async()
        yield ndb.get_multi_async([value.of_property for value in values])
        raise ndb.Return(values)

    """def has_pending_values(self):
        from models_property import PropertyValue
        q = PropertyValue.query(PropertyValue.room == self, PropertyValue.visible == False)
        if q.count(limit=1) > 0:
            return True
        return False"""

def room_exists(kid):
    return Room.get_by_id(int(kid))

def text_exists(kid):
    return Room.get_by_id(int(kid))