from google.appengine.ext import ndb

class User(ndb.Model):
    user = ndb.UserProperty()
    #user_id = ndb.StringProperty() This is now the key
    name = ndb.StringProperty()
    penname = ndb.StringProperty()
    first_login = ndb.DateTimeProperty(auto_now_add=True)
    #last_login = ndb.DateTimeProperty(auto_now=True)
    contact = ndb.BooleanProperty()

    """ SETTING """

    def set_info(self, **info):
        self.penname = info['penname']
        self.name = info['realname']
        self.contact = info['contact']
        future = self.put_async()
        return future

    def delete_self(self, reason):
        # delete worlds recursive
        # delete access rights
        # delete unpublished texts
        # delete unpublished properties
        # delete saved journeys

        # delete async */| multi
        # ndb.delete_multi([m.key for m in list_of_model_instances])
        from palimpsest.models import Feedback
        if reason:
            new_fb = Feedback(userid=str(self.key), deleting=reason)
            fb_ftr = new_fb.put_async()
        self.key.delete()
        if reason:
            fb_ftr.get_result()

    """ GETTING """
    def printable(self):
        if self.penname:
            return self.penname
        elif self.name:
            return self.name
        else:
            return self.user.nickname()

    # World getting
    @ndb.tasklet
    def get_worlds(self):
        from models_world import World
        w = yield World.query(World.creator == self.key).fetch_async()
        raise ndb.Return(w)

    @ndb.tasklet
    def get_mod(self):
        from models_moderator import Moderator
        ms = yield Moderator.query(Moderator.user == self.key).fetch_async()
        yield ndb.get_multi_async([m.world for m in ms])
        raise ndb.Return(ms)

    @ndb.tasklet
    def get_open_worlds_not_by(self):
        from models_world import World
        w = yield World.query(World.creator != self.key, World.is_open == True).fetch_async()
        raise ndb.Return(w)

    @ndb.tasklet
    def get_works(self):
        worlds, mods, opens = yield self.get_worlds(), self.get_mod(), self.get_open_worlds_not_by()
        raise ndb.Return((worlds, mods, opens))

    # Access getting

    # Text (Room) getting

    # Properties getting

def get_google_user_key(g_user):
    return User.query(User.user == g_user).fetch_async(keys_only=True)

def user_exists_bykey(key):
    return key.get()
    
class UserPending(ndb.Model):
    email = ndb.StringProperty()

    @ndb.tasklet
    def get_pending_mods(self):
        from models_moderator import Moderator
        m = yield Moderator.query(Moderator.user_pending == self.key).fetch_async()
        raise ndb.Return(m)

