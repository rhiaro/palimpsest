from google.appengine.ext import ndb
from models_user import User, UserPending
from models_world import World

class Moderator(ndb.Model):
    added_by = ndb.KeyProperty(kind=User)
    user = ndb.KeyProperty(kind=User) #has_access
    user_pending = ndb.KeyProperty(kind=UserPending) #will_have_access
    world = ndb.KeyProperty(kind=World) #contributors
    admin = ndb.BooleanProperty() # True means can edit world values and moderators, False can only affect rooms, etc