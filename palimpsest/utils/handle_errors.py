import web
from palimpsest.pages.page import render

def notfound():
    return web.notfound(render().msg_page("404 Page not found", "We lost this page, or maybe it never existed.  Never mind, eh?"))

def internalerror():
    return web.internalerror(render().msg_page("Uh oh!", "Something went wrong on the server.  Try again.  If this persists, please send us some <a href='/feedback'>feedback</a>."))

def forbidden():
	return web.forbidden(render().msg_page("What're you up to?", "This is a 403.  That means you're not allowed here.  Go on, try somewhere else."))

def user_disallowed():
	return render().msg_page("What're you up to?", "You're not allowed here.  Go on, try somewhere else.")