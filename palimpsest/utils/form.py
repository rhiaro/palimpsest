from web.form import Form as BaseForm
from web.form import Input
from web import utils, net

class Submit(Input):
    """ HTML Submit button """

    def is_hidden(self):
        return True # Because otherwise it outputs an empty label for the damn thing

    def __init__(self, name, *validators, **attrs):
        super(Submit, self).__init__(name, *validators, **attrs)
        self.description = ""

    def render(self):
        attrs = self.attrs.copy()
        attrs['name'] = self.name
        if self.value is not None:
            attrs['value'] = self.value
        return '<input type="submit" %s />' % attrs

class Email(Input):
    """ HTML5 Email input """

    def render(self):
        attrs = self.attrs.copy()
        attrs['name'] = self.name
        if self.value is not None:
            attrs['value'] = self.value
        return '<input type="email" %s />' % attrs

class Date(Input):
    """ HTML5 Date input """

    def render(self):
        attrs = self.attrs.copy()
        attrs['name'] = self.name
        if self.value is not None:
            attrs['value'] = self.value
        return '<input type="date" %s />' % attrs


class Form(BaseForm):
    def __init__(self, *inputs, **kwargs):
        if "klass" in kwargs:
            kwargs["class"] = kwargs["klass"]
            del kwargs["klass"]
        super(Form, self).__init__(*inputs, **kwargs)
        self._pre_form = kwargs.pop('pre_form', None)
        self._post_form = kwargs.pop('post_form', None)
        self._pre_input = kwargs.pop('pre_input', None)
        self._post_input = kwargs.pop('post_input', None)

    def render(self):
        out = ''

        if self._pre_form is not None:
            out += self._pre_form(self)

        out += self.render_form()

        if self._post_form is not None:
            out += self._post_form(self)

        return out

    def render_form(self):
        """ So named as it won't call the pre_form or post_form method """
        out = ''
        for i in self.inputs:
            html = utils.safeunicode(i.pre) + i.render() + utils.safeunicode(i.post)

            if self._pre_input is not None:
                html = self._pre_input(i) + html

            if self._post_input is not None:
                html = html + self._post_input(i)

            if i.is_hidden():
                out += '<p>%s</p>\n' % html
            else:
                out += '<p><label for="%s">%s</label>%s</p>\n' % (i.id, net.websafe(i.description), html)
        return out

def display_input_warning(i):
    if i.note:
        return "<span class=\"fail\">%s</span>" % net.websafe(i.note)
    else:
        return ""

def display_form_warning(f):
    if f.note:
        return '<div class="fail-bg"><span class="fail">%s</span></div>' % net.websafe(f.note)
    else:
        return ""

def ensureIsForm(form):
    if not issubclass(form.__class__, BaseForm):
        raise TypeError("Combined Form needs web.py form or derivative")


class CombinedForm(object):
    def __init__(self, forms=[], **attrs):
        for form in forms:
            ensureIsForm(form)
        self._forms = forms
        self.attrs = attrs

    def addForm(self, form):
        ensureIsForm(form)
        self._forms.append(form)

    def render(self, withFormTags=True):
        pre = ''
        middle = ''
        post = ''
        for form in self._forms:
            if form._pre_form is not None:
                pre += form._pre_form(form)
            middle += form.render_form()
            if form._post_form is not None:
                post += form._post_form(form)
        out = ("%s<form%s>\n%s\n</form>%s\n" % (pre, self.renderAttrs(), middle, post)) if withFormTags else html
        return out

    def renderAttrs(self):
        out = ''
        for attrName, attrVal in self.attrs.items():
            out += ' %s="%s"' % (attrName, attrVal)
        return out
