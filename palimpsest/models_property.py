from google.appengine.ext import ndb
from models_user import User
from models_world import World
from models_room import Room

class Property(ndb.Model):
    name = ndb.StringProperty()
    sorter = ndb.BooleanProperty() # User can elect to filter rooms by it
    info = ndb.BooleanProperty() # Shows up in 'additional info' about room
    discrete = ndb.BooleanProperty() # True = sliding scale, False = checkboxes. TODO: Add option for radio?
    valid_values = ndb.StringProperty() # |text|, |num| or comma separated string of allowed values defined by creator
    world = ndb.KeyProperty(kind=World) #has_properties
    added_by = ndb.KeyProperty(kind=User) #properties_added
    last_modified_by = ndb.KeyProperty(kind=User) #properties_modified
    date_added = ndb.DateTimeProperty(auto_now_add=True)
    last_modified_date = ndb.DateTimeProperty(auto_now=True)
    visible = ndb.BooleanProperty() # If True for public submission, implicitly 'accepted'
    rejected = ndb.BooleanProperty() # If False for public submission, status is 'pending'
    rejected_from = ndb.KeyProperty(kind=World) #rejected_properties'

    pending = ndb.ComputedProperty(lambda self: self.is_pending())
    #moderated = ndb.ComputedProperty(lambda self: self.is_moderated())

    def is_made_by(self, u):
        return self.added_by == u.key

    def get_input_type(self):
        if self.valid_values == "|text|" or self.valid_values == "|num|":
            property_input = "<input type=\"text\" name=\"prop_val\" id=\"prop_val\" style=\"display:inline; max-width:16em\"/>"
        else:
            vals = self.valid_values.split('|')
            property_input = "<select id=\"prop_val\" name=\"prop_val\">"
            for value in vals:
                property_input += "<option value=\"%s\">%s</option>" % (value, value)
            property_input += "</select>"
        return property_input

    def is_pending(self):
        return self.visible == False and self.rejected == False

    def in_world(self, world):
        return self.world.key.string_id() == world.key.string_id()

    def useable_by(self, user):
        return self.visible or (self.is_made_by(user) and not self.rejected)

    def changeable_by(self, user):
        """ Can edit/delete if mod OR (world open AND user made self AND not visible) """
        return self.world.is_mod(user) or (self.world.is_open and (not self.visible) and (not self.rejected) and self.added_by.user == user.key)


def property_exists(pid):
    return Property.get_by_id(int(pid))

class PropertyValue(ndb.Model):
    value = ndb.StringProperty()
    of_property = ndb.KeyProperty(kind=Property) #values
    room = ndb.KeyProperty(kind=Room) #property_values
    added_by = ndb.KeyProperty(kind=User) #created_values
    date_added = ndb.DateTimeProperty(auto_now_add=True)
    visible = ndb.BooleanProperty()

    # TODO: Make these async:

    @classmethod
    def delete_all_for_property(cls, prop):
        pvs = cls.query(cls.of_property == prop.key).fetch_async(keys_only = True).get_result()
        for pv in pvs:
            pv.delete()

    @classmethod
    def delete_all_for_text(cls, text):
        pvs = cls.query(cls.room == text.key).fetch_async(keys_only = True).get_result()
        for pv in pvs:
            pv.delete()