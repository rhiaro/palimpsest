from google.appengine.ext import ndb
from models_user import User

class World(ndb.Model):
    title = ndb.StringProperty()
    blurb = ndb.TextProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)
    creator = ndb.KeyProperty(kind=User) #worlds
    cover = ndb.StringProperty()
    publish = ndb.BooleanProperty()
    void_text = ndb.TextProperty()
    is_open = ndb.BooleanProperty()

    @classmethod
    def _get_default_world(klass):
        user_future = World.get_or_insert_async("default-world", title="Untitled Experience")
        return user_future.get_result()

    """ TESTING """
    def is_made_by(self, u):
        return self.creator == u.key

    def is_admin(self, u):
        from models_moderator import Moderator
        mod  = Moderator.query(Moderator.world == self.key, Moderator.user == u.key, Moderator.admin == True).fetch_async(keys_only=True).get_result()
        return self.is_made_by(u) or len(mod) > 0

    def is_mod(self, u): # Does this word accept unmoderated contributions from u?
        from models_moderator import Moderator
        mod  = Moderator.query(Moderator.world == self.key, Moderator.user == u.key).fetch_async(keys_only=True).get_result()
        return self.is_admin(u) or len(mod) > 0

    """ GETTING """
    def get_room_keys(self):
        from models_room import Room
        return Room.query(Room.world == self.key).fetch_async(keys_only=True)

    def get_properties_keys(self):
        from models_property import Property
        return Property.query(Property.world == self.key).fetch_async(keys_only=True)

    @ndb.tasklet
    def get_properties(self):
        from models_property import Property
        p = yield Property.query(Property.world == self.key).fetch_async()
        raise ndb.Return(p)

    @ndb.tasklet
    def get_rooms(self):
        from models_room import Room
        r = yield Room.query(Room.world == self.key).fetch_async()
        raise ndb.Return(r)

    @ndb.tasklet
    def get_everything(self):
        p, r = yield self.get_properties(), self.get_rooms()
        #to mod
        raise ndb.Return((p, r))

    @ndb.tasklet
    def get_properties_allowed(self, user):
        from models_property import Property
        qry = Property.query(ndb.AND(Property.world == self.key,
                             ndb.OR(Property.visible == True, Property.added_by == user.key)
                                    )
                            )
        p = yield qry.fetch_async()
        raise ndb.Return(p)

    @ndb.tasklet
    def get_rooms_allowed(self, user):
        from models_room import Room
        qry = Room.query(ndb.AND(Room.world == self.key,
                             ndb.OR(Room.visible == True, Room.added_by == user.key)
                                    )
                            )
        r = yield qry.fetch_async()
        raise ndb.Return(r)

    @ndb.tasklet
    def get_everything_allowed(self, user):
        p, r = yield self.get_properties_allowed(user), self.get_rooms_allowed(user)
        raise ndb.Return((p, r))

    @ndb.tasklet
    def get_moderators(self):
        from models_moderator import Moderator
        m = yield Moderator.query(Moderator.world == self.key).fetch_async()
        raise ndb.Return(m)

    def get_access_string(self, user):
        if self.is_admin(user):
            return "admin"
        if self.is_mod(user):
            return "closed"
        if self.is_open == True:
            return user.user.user_id()
        else:
            return "read"

    """ SETTING """

def get_default_void():
    return "You have entered the void. Keep walking to find your nearest location of interest."

def world_exists(kn):
    return World.get_by_id(kn)

def get_everything_by(user):
    worlds_by = World.query(World.creator == user.key).fetch_async()
    worlds_unmod_by = World.query(World.creator != user.key, World.is_open == False).fetch_async()
    worlds_open_not_by = World.query(World.creator != user.key, World.is_open == True).fetch_async()

    return (worlds_by, worlds_unmod_by, worlds_open_not_by)
